Ability: detect duplicate files

  As the owner of a repository of files
  I want to know which files are duplicated
  So that I can reduce the amount of duplication in my repository.

  The process below implies a computation complexity of O(n),
  as each file only needs to be compared against the keys of a hash table,
  something which is designed to be O(1).

  Rule: files with different size are never duplicates

    Scenario: quick uniqueness check
      Given 2 files with different sizes
       When the files are compared for uniqueness
       Then they should be deemed to be unique

    Scenario: quick duplicate check
      Given 2 files with the same size
       When the files are compared for uniqueness
       Then they should be deemed to be as possible duplicates
        And they should be checked for identical hashes

  Rule: files with the same size and same hash are duplicates

    Scenario: only hash files which share the same size with at least 1 other file
      Given 2 files with the same size
       When the files are compared for uniqueness
       Then a hash digest of each file should be made of all files with the same size

    Scenario: files with same size but different hash
      Given 2 files with the same size
        And a hash digest of each file is available
       When the hash of each file is different
       Then the files are unique

    Scenario: files with same size but different hash
      Given 2 files with the same size
        And a hash digest of each file is available
       When the hash of each file is different
       Then the files are unique

    Scenario: file uniqueness checks
      Given 2 files with <file a size> and <file b size>
        And a hash digest of each file is available
       When the hash of each file is different
       Then the files are unique

            | file a size | file a hash | file b size | file b hash | need hash | uniqueness |
            | ----------- | ----------- | ----------- | ----------- | --------- | ---------- |
            | 100         |             | 123         |             | no        | unique     |
            | 100         |             | 100         |             | yes       | unknown    |
            | 100         | abc         | 100         | def         |           | unique     |
            | 100         | abc         | 100         | abc         |           | duplicate  |
