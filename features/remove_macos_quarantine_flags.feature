Ability: remove Mac OS's `quarantine` flag

  As the owner of a repository of files
  I want to remove Apple's `quarantine` flag from all of my documents
  So that I can use the documents without waiting for apple to approve their use.

  For years, Apple has been adding so-called `quarantine` flags to files
  downloaded from the internet. When Mac OS X detects this flag, it laboriously
  checks the content of the file for "malicious signatures", indications that
  the files contain some known kind of known.

  Especially when viewing large videos over a network, this process can take
  several minutes to complete, and there are no guarantees that all malware
  would be detected by this process.

  The cost-benefit ratio is so high that I find it more convenient to remove
  the quarantine flag, allowing all documents to be viewed as they were
  intended.

  Of course, removal of the quarantine flag must be optional, and it must be
  possible to store the preference that particular files should/should not be
  de-quarantined.
