Ability: index all files

  As the owner of a repository of files
  I want to index all files and their tags
  So that I can sort, filter and find any file quickly.
