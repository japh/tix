Ability: get the names of all unique files

  As the owner of a repository of files
  I want to get a list of all of the files, based on their content
  So that I do not have to consider any file twice.


