Ability: get the names of all non-unique files

  As the owner of a repository of files
  I want to get a list of all files which have at least 1 duplicate
  So that I can combine their meta data (tags) and remove duplicates.


