Ability: store file index

  As the owner of a repository of files
  I want to store the index of files and tags
  So that tix can be used without having to scan the repository every time.
