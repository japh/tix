@wip
Ability: colorise tags

  As a person who tags files
  I want to be able set the color of some tags
  To make important tags stand out

  The use of color can help indicate emotions related to a word,
  for example "green" is generally understood to be "good" or
  "positive" while "red" indicates "bad", "negative" or "danger".

  Rule: tix only provides a limited set of colors

    Note: this color selection is derived from Apple Mac OS, but tix does not
    use Apple's numerical representation. The actual colors used (RGB, HSL etc) are left to the client to define.

    Scenario: test all colors defined by tix
      Given a tag named "Test"
       When the "Test" tag's color is set to "<set color>"
       Then the "Test" tag's color should be "<expect color>"

      Examples:
        | set color | expect color | comments                                                   |
        | none      | none         | default, no color                                          |
        | red       | red          |                                                            |
        | Orange    | orange       | color names are always translated to their lower-case form |
        | YELLOW    | yellow       |                                                            |
        | green     | green        |                                                            |
        | blue      | blue         |                                                            |
        | purple    | purple       |                                                            |
        | grey      | grey         |                                                            |
        | beige     | none         | unknown colors are not retained                            |

  Rule: tags have no color by default

    Scenario: an uncolored tag
      Given a tag named "Test"
       Then the tag should have the color "none"

  Rule: a tag may only have exactly 1 color

    Scenario: set the color of a tag
      Given a tag named "Test"
       When the "Test" tag's color is set to "red"
       Then the "Test" tag's color should be "red"

    Scenario: change the color of a tag
      Given a tag named "Test"
       When the "Test" tag's color is set to "red"
        And the "Test" tag's color is set to "green"
       Then the "Test" tag's color should be "green"

    Scenario: remove the color of a tag
      Given a tag named "Test"
       When the "Test" tag's color is set to "red"
        And the "Test" tag's color is set to "none"
       Then the "Test" tag's color should be "none"

  Rule: setting an unknown color causes the color "none" to be set

    Scenario: setting an unknown tag color
      Given a tag named "Test"
       When the tag's color is set to "beige"
       Then the tag should have the color "none"
 
