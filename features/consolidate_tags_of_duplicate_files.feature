Ability: consolidate the tags of duplicated files

  As the owner of a repository of files
  I want to ensure that all duplicates of a file have the same set of tags
  So that I can delete any of the duplicates without losing meta data about
  their content.

  Rule: all duplicate files should have the same set of tags

    All of the files in the examples here have the identical content, and are
    therefor deemed to be duplicates of each other.

    No assumptions are made about which file is "best", they should all end up
    with the same set of tags.

    Scenario: merge empty tag lists
      Given duplicate files with the tags
            | file       | tags  |
            | ---------- | ----- |
            | old.txt    |       |
            | recent.txt |       |
            | new.txt    |       |
       When the tags are merged
       Then the duplicate files should have the tags
            | tags |
            | ---- |
            |      |

    Scenario: single tag list
      Given duplicate files with the tags
            | file     | tags  |
            | -------- | ----- |
            | file.txt | c a b |
       When the tags are merged
       Then the duplicate files should have the tags
            | tags  |
            | ----- |
            | c a b |

    Scenario: merge identical tag sets
      Given duplicate files with the tags
            | file       | tags  |
            | ---------- | ----- |
            | old.txt    | b a c |
            | recent.txt | b a c |
            | new.txt    | b a c |
       When the tags are merged
       Then the duplicate files should have the tags
            | tags  | comments                               |
            | ----- | -------------------------------------- |
            | b a c | all tag lists were tha same, no change |

    Scenario: one tag list followed by empty tag lists
      Given duplicate files with the tags
            | file       | tags  |
            | ---------- | ----- |
            | old.txt    | c a b |
            | recent.txt |       |
            | new.txt    |       |
       When the tags are merged
       Then the duplicate files should have the tags
            | tags  | comment         |
            | ----- | --------------- |
            | c a b | only 1 tag list |

    Scenario: one tag list in the middle of a tag set
      Given duplicate files with the tags
            | file       | tags  |
            | ---------- | ----- |
            | old.txt    |       |
            | recent.txt | c a b |
            | new.txt    |       |
       When the tags are merged
       Then the duplicate files should have the tags
            | tags       | comment         |
            | ---------- | --------------- |
            | c a b      | only 1 tag list |

    Scenario: one tag list at the end of a tag list
      Given duplicate files with the tags
            | file       | tags  |
            | ---------- | ----- |
            | old.txt    |       |
            | recent.txt |       |
            | new.txt    | c a b |
       When the tags are merged
       Then the duplicate files should have the tags
            | tags       | comment         |
            | ---------- | --------------- |
            | c a b      | only 1 tag list |

    Scenario: merge independent tag sets

      Note that more recently used tags are given higher priority!

      Given duplicate files with the tags
            | file       | tags |
            | ---------- | ---- |
            | old.txt    | a    |
            | recent.txt | b    |
            | new.txt    | c    |
       When the tags are merged
       Then the duplicate files should have the tags
            | tags  | comments          |
            | ----- | ----------------- |
            | c b a | most recent first |

    Scenario: merge common subsets of tags

      Note that each tag _tries_ to maintain the same position in each set of
      tags.

      The longer the list of tags, the more important the first tags are
      assumed to be.

      The priority is approximatley:

      ( # tags - tag position ) * age factor

      Given duplicate files with the tags
            | file       | tags  |
            | ---------- | ----- |
            | old.txt    | a b   |
            | recent.txt | c a b |
            | new.txt    | a     |
       When the tags are merged
       Then the duplicate files should have the tags
            | tags  |
            | ----- |
            | a c b |

    Scenario: prioritise more recent tag positions

      A tag's position in each tag list represents it's priority, or
      importance.

      If a tag always appears at the beginning of a tag list, then it should
      appear at the beginning of the merged tag list. Likewise for tags that
      appear in the middle or end of each tag list.

      Since tags are unlikely to appear at the same position in each list, tix
      tracks a time-based rolling average of each tag's position, favouring
      later positions over older ones.

      Given duplicate files with the tags
            | file       | tags  |
            | ---------- | ----- |
            | old.txt    | a b c |
            | recent.txt | a b c |
            | new.txt    | c a b |
       When the tags are merged
       Then the duplicate files should have the tags
            | tags  |
            | ----- |
            | c a b |
