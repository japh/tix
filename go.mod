module codeberg.org/japh/tix

go 1.21

require (
	codeberg.org/japh/go-sqlite-schema v0.0.2
	codeberg.org/japh/id64 v0.1.2
	codeberg.org/japh/is v0.1.2
	github.com/carlmjohnson/versioninfo v0.22.5
	github.com/mattn/go-sqlite3 v1.14.19
	github.com/pkg/xattr v0.4.9
	golang.org/x/crypto v0.4.0
	howett.net/plist v1.0.0
)

require golang.org/x/sys v0.4.0 // indirect
