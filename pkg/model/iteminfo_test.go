package model

import (
	"encoding/json"
	"testing"

	"codeberg.org/japh/id64"
	"codeberg.org/japh/is"
)

var testItemInfos = map[string]*ItemInfo{
	"nil":     nil,
	"empty":   &ItemInfo{},
	"id only": &ItemInfo{ItemID: id64.FromString("Eat-Veggies")},
}

func TestModelMarshalling(t *testing.T) {
	is := is.New(t)

	const (
		nameCol = iota
		jsonCol
	)
	testCases := is.TableFromString(`
		| name    | json                     |
		| ------- | ------------------------ |
		| nil     | null                     |
		| empty   | {}                       |
		| id only | {"itemId":"Eat-Veggies"} |
		`)

	for _, tc := range testCases.DataRows()[1:] {
		testName := tc[nameCol]
		inItem := testItemInfos[testName]
		wantJson := tc[jsonCol]
		gotJson, err := json.Marshal(inItem)
		is.NotError(err)
		is.Equal(string(gotJson), wantJson, "%s item as JSON %#+v", testName, string(gotJson))
	}

}
