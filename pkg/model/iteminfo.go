package model

import (
	"time"

	"codeberg.org/japh/id64"
)

type ItemInfo struct {
	ItemID id64.ID64            `json:"itemId,omitempty"`
	Path   string               `json:"path,omitempty"`
	Name   string               `json:"name,omitempty"`
	Tags   []string             `json:"tags,omitempty"`
	Time   map[string]time.Time `json:"time,omitempty"`
	Link   map[string]string    `json:"links,omitempty"`
	Unique map[string]string    `json:"unique,omitempty"`
}
