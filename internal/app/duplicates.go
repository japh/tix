package app

import (
	"sort"
)

func (t *Tix) duplicateSizedFiles(allItems []*Item) map[string]*Item {

	duplicateSizedFiles := map[string]*Item{}

	if len(allItems) <= 1 {
		return duplicateSizedFiles
	}

	// clone the provided list of items so we can sort them without side
	// effects
	items := make([]*Item, 0, len(allItems))
	for _, f := range allItems {
		items = append(items, f)
	}

	// sort items by size - thus grouping items with same size
	sort.Slice(items,
		func(i, j int) bool {
			a := items[i]
			b := items[j]

			return a.Size > b.Size
		})

	// find groups of items with same size, but no common hash
	blockStart := 0
	blockAlgo := map[string]int{}
	for i, _ := range items[1:] {
		a := items[i]   // 0, 1, ...
		b := items[i+1] // 1, 2, ...

		if a.Size == b.Size {
			// record all hash algorithms available on all items with this size
			if blockStart == i {
				for algo := range a.Hash {
					blockAlgo[algo] += 1
				}
			}
			for algo := range b.Hash {
				blockAlgo[algo] += 1
			}
		} else {
			if blockSize := i + 1 - blockStart; blockSize > 1 {
				ok := false
				for _, c := range blockAlgo {
					if c == blockSize {
						ok = true
						break
					}
				}
				if !ok {
					for _, i := range items[blockStart : i+1] {
						duplicateSizedFiles[i.Path] = i
					}
				}
				// TODO: replace with clear() when go1.21 is available
				clearAlgoMap(blockAlgo)
			}

			blockStart = i + 1
		}
	}

	return duplicateSizedFiles
}

func clearAlgoMap(m map[string]int) {
	for k := range m {
		delete(m, k)
	}
}
