package app

import (
	"fmt"
	"time"

	"codeberg.org/japh/id64"
	"codeberg.org/japh/tix/internal/app/tag"
)

type Item struct {
	ItemID id64.ID64         // random ID
	Path   string            // full path used to identify the file
	Name   string            // basename of the file
	Size   int64             // in bytes
	MTime  time.Time         // modification time
	CTime  time.Time         // inode change time (only on unix)
	Hash   map[string]string // any number of hashes, with 1 per algorithm
	Tags   tag.List          // ordered list of tags assiged to this item
}

func NewItem() *Item {
	i := &Item{}

	i.ItemID = id64.RandomID()
	i.Hash = map[string]string{}

	return i
}

func (i *Item) String() string {
	return fmt.Sprintf("Item: %s (%d B, %d Tags)", i.Name, i.Size, len(i.Tags))
}
