package scaler_test

import (
	"fmt"
	"math"

	"codeberg.org/japh/tix/internal/app/scaler"
)

func ExampleScaledFloat64() {

	testUnits := []string{
		"B",
		"B/s",
		"g",
	}

	for _, u := range testUnits {
		for p := 0; p < 15; p += 2 {
			n := math.Pow10(p)
			sn := scaler.ScaledFloat64(n, u)
			fmt.Printf("%v => %v %v\n", sn, int64(n), u)
		}
	}

	// Output:
	//   1 B => 1 B
	// 100 B => 100 B
	//  10 kB => 10000 B
	//   1 MB => 1000000 B
	// 100 MB => 100000000 B
	//  10 GB => 10000000000 B
	//   1 TB => 1000000000000 B
	// 100 TB => 100000000000000 B
	//   1 B/s => 1 B/s
	// 100 B/s => 100 B/s
	//  10 kB/s => 10000 B/s
	//   1 MB/s => 1000000 B/s
	// 100 MB/s => 100000000 B/s
	//  10 GB/s => 10000000000 B/s
	//   1 TB/s => 1000000000000 B/s
	// 100 TB/s => 100000000000000 B/s
	//   1 g => 1 g
	// 100 g => 100 g
	//  10 kg => 10000 g
	//   1 Mg => 1000000 g
	// 100 Mg => 100000000 g
	//  10 Gg => 10000000000 g
	//   1 Tg => 1000000000000 g
	// 100 Tg => 100000000000000 g

}
