package scaler

import (
	"fmt"
	"math"
)

type scale struct {
	magnitude int
	prefix    string
}

var scales = []scale{
	{-9, "n"}, // nano
	{-6, "u"}, // micro (mu, μ)
	{-3, "m"}, // milli
	{0, ""},
	{3, "k"},  // kilo
	{6, "M"},  // Mega
	{9, "G"},  // Giga
	{12, "T"}, // Tera
	{15, "P"}, // Peta
}

// ScaledFloat64 returns the number provided
func ScaledFloat64(number float64, unit string) string {
	size := math.Abs(number)
	prefix := ""

	var si scale
	for _, s := range scales {
		l := math.Pow10(s.magnitude)
		if size < l*1000 {
			si = s
			break
		}
	}

	switch {
	case unit == "":
		if math.Abs(float64(si.magnitude)) > 3 {
			number /= math.Pow10(si.magnitude)
			prefix = fmt.Sprintf("x10^%d", si.magnitude)
		}
	default:
		number /= math.Pow10(si.magnitude)
		prefix = " " + si.prefix
	}

	return fmt.Sprintf("%3.0f%v%v",
		number,
		prefix,
		unit,
	)
}
