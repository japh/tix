package app

import (
	"fmt"
	"log"
	"os"
	"time"

	"codeberg.org/japh/id64"
	"codeberg.org/japh/tix/internal/adapter/apple"
	"codeberg.org/japh/tix/internal/app/progress"
	"codeberg.org/japh/tix/internal/app/scaler"
	"codeberg.org/japh/tix/internal/app/tag"
)

type Tix struct {
	maxHashers int
	store
	walker
	hasher
}

// Interfaces used by Tix

// walker traverses all files in a file system, collecting only the
// name and size of each file, via a (file) store
type walker interface {
	Walk(string, *progress.Bar) error
}

// (file) store persists file data
type store interface {
	Begin() error
	Commit() error
	RegisterItem(path string, size int64, mTime time.Time)
	RegisterItemTags(id id64.ID64, tags tag.List)
	RegisterItemHash(id id64.ID64, algo, hash string)
	AllItems() []*Item
}

type hasher interface {
	Algorithm() string
	HashFile(string) string
}

type tixOpt func(t *Tix)

func New(opts ...tixOpt) *Tix {
	t := &Tix{}

	for _, opt := range opts {
		opt(t)
	}

	return t
}

func WithWalker(w walker) tixOpt {
	return func(t *Tix) {
		t.walker = w
	}
}

func WithStore(s store) tixOpt {
	return func(t *Tix) {
		t.store = s
	}
}

func WithHasher(h hasher) tixOpt {
	return func(t *Tix) {
		t.hasher = h
	}
}

func WithMaxHashers(m int) tixOpt {
	return func(t *Tix) {
		t.maxHashers = m
	}
}

func (t *Tix) Walk(root string) {
	bar := progress.New(
		progress.WithWidth(20),
	)
	defer bar.Finished()

	bar.Reset(progress.WithTitle("Walking Directories"))
	t.walker.Walk(root, bar)
	bar.Finished()

	allItems := t.AllItems()
	log.Printf("got %d items from the database", len(allItems))
	var allBytes int64
	for _, i := range allItems {
		allBytes += i.Size
	}

	missingFiles := []*Item{}

	bar.Reset(
		progress.WithTitle("Scanning Tags"),
		progress.WithUnit(""),
		progress.WithTodo(int64(len(allItems))),
	)
	t.store.Begin()
	regCount := 0
	for _, i := range allItems {
		if _, err := os.Stat(i.Path); err != nil {
			missingFiles = append(missingFiles, i)
			continue
		}
		fsTags, err := apple.Tags(i.Path)
		bar.Did(1, "registered %d", regCount)
		if err != nil {
			continue
		}
		if !i.Tags.IsEqual(fsTags) {
			t.store.RegisterItemTags(i.ItemID, fsTags)
			regCount += 1
			bar.Did(0, "registered %d", regCount)
		}
	}
	t.store.Commit()
	bar.Finished()

	if len(missingFiles) > 0 {
		log.Printf("missing items:")
		for _, i := range missingFiles {
			log.Printf("  %s", i.Path)
		}
	}

	dupSizeFiles := t.duplicateSizedFiles(allItems)
	var todoBytes int64
	for _, i := range dupSizeFiles {
		todoBytes += i.Size
	}

	fmt.Printf("hashing %d of %d items with %s (%v of %v)\n",
		len(dupSizeFiles),
		len(allItems),
		t.hasher.Algorithm(),
		scaler.ScaledFloat64(float64(todoBytes), "B"),
		scaler.ScaledFloat64(float64(allBytes), "B"),
	)

	// ticker := time.NewTicker(time.Second)
	// defer ticker.Stop()

	pool := make(chan bool, t.maxHashers)
	doing := make(chan *Item, t.maxHashers)
	did := make(chan *Item, t.maxHashers)
	done := make(chan bool)

	bar.Reset(
		progress.WithTitle("Hashing Dups "),
		progress.WithUnit("B"),
		progress.WithTodo(todoBytes),
	)

	go func() {

		for _, i := range dupSizeFiles {
			pool <- true // block until a worker slot is available
			go func(i *Item) {
				doing <- i // let the progress bar know how much work we will do
				algo := t.hasher.Algorithm()
				hash := t.hasher.HashFile(i.Path)
				t.store.RegisterItemHash(i.ItemID, algo, hash)
				defer func() {
					did <- i // let the progress bar know how much work we did
					<-pool   // release our spot in the pool
				}()
			}(i)
		}

		// fmt.Println("waiting for stragglers...\n")
		for i := 0; i < cap(pool); i++ {
			pool <- true
		}

		done <- true
	}()

	timeoutCount := 0
progress:
	for {
		select {
		case i := <-doing:
			bar.Doing(i.Size, fmt.Sprintf("(%d) %s", len(pool), i.Name))
		case i := <-did:
			bar.Did(i.Size)
		case <-time.After(1 * time.Second):
			bar.Did(0)
			timeoutCount++
		// case <-ticker.C:
		//  bar.Did(0)
		case <-done:
			break progress
		}
	}
	bar.Finished("finished")

	fmt.Printf("got %d timeouts\n", timeoutCount)
}
