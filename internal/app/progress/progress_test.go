//go:build ignore

// TODO (Steve) How the ?!?!? should I test a progress bar?
package progress_test

import (
	"testing"
	"time"

	"codeberg.org/japh/tix/internal/app/progress"
)

func TestProgress(t *testing.T) {

	steps := 0.00000023
	runLength := 100.0
	stepDuration := 150 * time.Millisecond

	b := progress.New(
		progress.WithUnit("B"),
		progress.WithWidth(65),
	)

	b.Start()
	b.Todo(steps * runLength)
	for i := 0.0; i < runLength; i++ {
		b.Doing(steps, "step %v", i)
		time.Sleep(stepDuration)
		b.Did(steps)
	}
	b.AllDone()
}
