package progress

import (
	"fmt"
	"math"
	"strings"
	"time"

	"codeberg.org/japh/tix/internal/app/scaler"
)

const (
	ansiEL = "\x1b[K" // ANSI kill to end of line escape sequence
)

type Bar struct {
	isRunning bool      // flag to indicate whether the progress bar is currently running or not
	width     float64   // width of the progress bar
	todo      float64   // total number of steps to be done
	doing     float64   // number of steps currently being processed (in progress)
	done      float64   // number of steps already completed
	started   time.Time // the time that the current process was started
	unit      string    // the units being measured, e.g. "B" for "bytes" or "m" for meters
	title     string    // the name of the process being run
	message   string    // the message that appears after the progress bar
}

// New creates a new progress meter
func New(opts ...opt) *Bar {
	b := &Bar{
		width:   40,
		started: time.Now(),
	}

	for _, o := range opts {
		o(b)
	}

	return b
}

type opt func(*Bar)

func WithWidth(w int) opt   { return func(b *Bar) { b.width = float64(w) } }
func WithTodo(s int64) opt  { return func(b *Bar) { b.todo = float64(s) } }
func WithUnit(u string) opt { return func(b *Bar) { b.unit = u } }

func WithTitle(t ...any) opt {
	return func(b *Bar) {
		b.title = ""
		if len(t) > 0 {
			b.title = fmt.Sprintf(t[0].(string), t[1:]...) + " "
		}
	}
}

func WithMessage(m ...any) opt {
	return func(b *Bar) {
		b.message = ""
		if len(m) > 0 {
			b.message = fmt.Sprintf(m[0].(string), m[1:]...)
		}
	}
}

func (b *Bar) Reset(opts ...opt) {
	if b.isRunning {
		fmt.Println()
	}

	b.todo = 0
	b.doing = 0
	b.done = 0

	for _, o := range opts {
		o(b)
	}

	b.started = time.Now()
	b.renderProgress()
}

func (b *Bar) Start() {
	b.started = time.Now()
	b.renderProgress()
}

// Todo increases the amount of work to be done
func (b *Bar) Todo(size int64) {
	b.todo += float64(size)
	b.renderProgress()
}

// Doing indicates that some part of the work to be done has been started
func (b *Bar) Doing(size int64, message ...any) {
	b.doing += float64(size)
	b.message = ""
	if len(message) > 0 {
		b.message = fmt.Sprintf(message[0].(string), message[1:]...)
	}
	b.renderProgress()
}

// Did increases the amount of work which has been done
func (b *Bar) Did(size int64, message ...any) {
	delta := float64(size)
	b.doing -= math.Min(delta, b.doing)
	b.done += delta

	if len(message) > 0 {
		b.message = fmt.Sprintf(message[0].(string), message[1:]...)
	}

	b.renderProgress()
}

// Finished indicates that the task being measured has finished completely
func (b *Bar) Finished(message ...any) {
	if !b.isRunning {
		return
	}
	b.message = ""
	if len(message) > 0 {
		b.message = fmt.Sprintf(message[0].(string), message[1:]...)
	}
	b.renderProgress()
	fmt.Println()
	b.isRunning = false
}

func (b *Bar) renderProgress() {

	fmt.Printf("\r%v", b.title)

	if b.todo > 0 {
		b.renderBar()
	}

	b.renderStats()
}

func (b *Bar) renderBar() {
	width := int(b.width)
	doneTicks := int(b.done/b.todo*b.width + 0.5)
	doingTicks := int(b.doing/b.todo*b.width + 0.5)
	todoTicks := width - doneTicks - doingTicks

	// sanity checks
	doingTicks = inRange(0, doingTicks, width)
	doneTicks = inRange(0, doneTicks, width-doingTicks)
	todoTicks = inRange(0, todoTicks, width-doingTicks-doneTicks)

	fmt.Printf("[%s%s%s] ",
		strings.Repeat("#", doneTicks),
		strings.Repeat(".", doingTicks),
		strings.Repeat(" ", todoTicks),
	)
}

func inRange(min, value, max int) int {
	switch {
	case value < min:
		value = min
	case value > max:
		value = max
	}
	return value
}

func (b *Bar) renderStats() {
	var (
		remainingZero = "?"
		timeElapsed   = time.Since(b.started)
		timeRemaining time.Duration
		timeTotal     time.Duration
		rate          float64
	)

	if timeElapsed > 200*time.Millisecond {
		rate = b.done / timeElapsed.Seconds()
		if rate > 0 {
			remainingZero = "0"
			timeTotal = time.Duration(b.todo/rate) * time.Second
			timeRemaining = time.Duration((b.todo-b.done)/rate) * time.Second
		}
	}

	mwidth := 50
	mlen := len(b.message)
	if mlen > mwidth {
		b.message = b.message[0:mwidth/2-2] + "..." + b.message[mlen-mwidth/2:]
	}

	switch {

	case b.todo > 0:
		fmt.Printf("%v in %v @ %v/s eta: ~%v of ~%v %s%s",
			scaler.ScaledFloat64(b.done, b.unit),
			durationString(timeElapsed, "0"),
			scaler.ScaledFloat64(rate, b.unit),
			durationString(timeRemaining, remainingZero),
			durationString(timeTotal, "?"),
			b.message,
			ansiEL,
		)

	default:
		fmt.Printf("%v in %v @ %v/s %s%s",
			scaler.ScaledFloat64(b.done, b.unit),
			durationString(timeElapsed, "0s"),
			scaler.ScaledFloat64(rate, b.unit),
			b.message,
			ansiEL,
		)
	}

	b.isRunning = true
}

func durationString(d time.Duration, zero string) string {
	if d == 0 {
		return zero + "s"
	}
	return d.Round(time.Second).String()
}
