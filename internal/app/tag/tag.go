package tag

import (
	"fmt"
	"strings"
)

// Tag represents a single tag, with a name and color
type Tag struct {
	id    string // case-independent copy of name
	name  string
	color string
}

// New creates a new tag with a name and color.
//
// The color may be a color id ("0" .. "7") or name ("none", "red", "orange",
// "yellow", "green", "blue", "purple", "grey").
// Any other color strings will be translate to the color None
//
// An empty string may be provided for either name or color, however, nil will
// be returned if neither name nor color are defined.
func New(name, color string) *Tag {

	if name == "" {
		return nil
	}

	t := &Tag{
		id:   strings.ToLower(name),
		name: name,
	}

	// ensure that we only have valid colors
	t.SetColor(color)

	return t
}

// ID returns a lower-case representation of a tag, with all non-alphanumeric characters replaced by spaces.
func (t *Tag) ID() string { return t.id }

// Name returns the name of the tag
func (t *Tag) Name() string { return t.name }

// Color returns the color of the tag as a simple string
func (t *Tag) Color() string { return t.color }

// String provides a string containing the tag's name and the name of its color
func (t *Tag) String() string {
	if t.HasColor() {
		return fmt.Sprintf("@(%s)[%s]", t.name, t.color)
	}
	return fmt.Sprintf("@(%s)", t.name)
}
