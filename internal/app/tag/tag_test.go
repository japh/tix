package tag_test

import (
	"fmt"
	"testing"

	"codeberg.org/japh/is"
	"codeberg.org/japh/tix/internal/app/tag"
)

func TestTagCreation(t *testing.T) {
	is := is.New(t)

	const (
		haveNameCol = iota
		haveColorCol
		wantIdCol
		wantNameCol
		wantColorCol
		wantStringCol
		commentsCol
	)
	testCases := is.TableFromString(`
		| tag | color  | id | name | color  | string       | comments          |
		| --- | ------ | -- | ---- | ------ | ------------ | ----------------- |
		| a   |        | a  | a    | none   | @(a)         |                   |
		| B   | groan  | b  | B    | none   | @(B)         | not a valid color |
		| c   | purple | c  | c    | purple | @(c)[purple] |                   |
		| Hi  | RED    | hi | Hi   | red    | @(Hi)[red]   |                   |
		`)

	for _, tc := range testCases.DataRows()[1:] {
		fn := fmt.Sprintf("%s(%s)", tc[haveNameCol], tc[haveColorCol])
		t := tag.New(tc[haveNameCol], tc[haveColorCol])
		is.Equal(t.ID(), tc[wantIdCol], "tag %s id => %q", fn, tc[wantIdCol])
		is.Equal(t.Name(), tc[wantNameCol], "tag %s name => %q", fn, tc[wantNameCol])
		is.Equal(t.Color(), tc[wantColorCol], "tag %s color => %q", fn, tc[wantColorCol])
		is.Equal(t.String(), tc[wantStringCol], "tag %s string => %q", fn, tc[wantStringCol])
	}
}

func TestTagColorChange(t *testing.T) {
	is := is.New(t)

	const (
		oldCol = iota
		newCol
		wantCol
	)
	testChanges := is.TableFromString(`
		| old    | new         | result |
		| ------ | ----------- | ------ |
		| none   | none        | none   |
		| none   | red         | red    |
		| green  | none        | none   |
		| blue   | not a color | none   |
		| yellow | Orange      | orange |
		| purple | GREY        | grey   |
		`)

	for _, tc := range testChanges.DataRows()[1:] {
		oldColor := tc[oldCol]
		newColor := tc[newCol]
		wantColor := tc[wantCol]

		tag := tag.New("test", oldColor)
		tag.SetColor(newColor)
		is.Equal(tag.Color(), wantColor, "change tag color from %q to %q => %q", oldColor, newColor, wantColor)
	}
}
