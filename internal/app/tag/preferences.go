package tag

type Preferences struct {
	tagWithID map[string]*Tag
}

func NewPreferences() *Preferences {
	return &Preferences{
		tagWithID: map[string]*Tag{},
	}
}

func (p *Preferences) NewTag(name, color string) *Tag {
	tag := New(name, color)
	id := tag.ID()
	if name == id {
		pref, exists := p.tagWithID[id]
		if exists {
			tag = pref
		}
	} else {
		p.tagWithID[id] = tag
	}
	return tag
}
