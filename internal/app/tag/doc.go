//
// Package tag provides a representation for short, descriptive words or
// phrases, used to describe the contents of a file.
//
// The combination of multiple tags can help to uniquely classify files.
//
// TIX's primary goal is management of duplicate files, including the alignment
// of their tags.
//
// Since file tagging is a manual, almost random task, it is highly
// unlikely that all duplicates of a file will have the same set of tags.
// Some tags can be expected to be common to all duplicates, others may
// represent aspects of the file which were not considered while tagging the
// other duplicates.
//
// There is also the consideration of inconsistent spelling, capitalisation or
// colorisation of tags.
//
// This package provides:
//
//  - basic tag attributes: text and color
//
//  - merging the tags of multiple, identical files into a single tag set
//    - while maintaining a (hopefully) recognisable order
//
//  - identification and alignment of inconsitently spellt tags
//
//  - the permanent replacement of tags
//    - e.g. always replace 'important' with 'prio 1'
//
package tag
