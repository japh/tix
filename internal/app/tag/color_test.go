package tag_test

import (
	"testing"

	"codeberg.org/japh/is"
	"codeberg.org/japh/tix/internal/app/tag"
)

func TestAllTagColors(t *testing.T) {
	is := is.New(t)

	const (
		haveNameCol = iota
		wantNameCol
		commentsCol
	)
	tagColors := is.TableFromString(`
		| have   | want   | comment                     |
		| ------ | ------ | --------------------------- |
		|        | none   | no color => 'none'          |
		| ------ | ------ | --------------------------- |
		| none   | none   | defined tag colors          |
		| red    | red    |                             |
		| orange | orange |                             |
		| yellow | yellow |                             |
		| green  | green  |                             |
		| blue   | blue   |                             |
		| purple | purple |                             |
		| grey   | grey   |                             |
		| ------ | ------ | --------------------------- |
		| GREEN  | green  | colors are case-insensitive |
		| Blue   | blue   |                             |
		| PuRPlE | purple |                             |
		| grEY   | grey   |                             |
		| Cheese | none   | invalid color => none       |
		`)

	for _, tc := range tagColors.DataRows()[1:] {
		haveName := tc[haveNameCol]
		wantName := tc[wantNameCol]

		gotName := tag.ValidColorName(haveName)
		is.Equal(gotName, wantName, "tag color name %q => %q", haveName, wantName)
	}
}
