package tag_test

import (
	"testing"

	"codeberg.org/japh/is"
	"codeberg.org/japh/tix/internal/app/tag"
)

func TestNoPreferences(t *testing.T) {
	is := is.New(t)

	testTags := is.TableFromString(`
		| name | preferred | comments                               |
		| ---- | --------- | -------------------------------------- |
		| foo  | foo       | all lowercase, no preference           |
		| Foo  | Foo       | upper case, set preference             |
		| foo  | Foo       | all lowercase, use previous preference |
		| FOO  | FOO       | all uppercase, set preference          |
		| foo  | FOO       | all lowercase, use previous preference |
		| bar  | bar       |                                        |
		`)

	p := tag.NewPreferences()

	for _, tt := range testTags.DataRows()[1:] {
		nameIn := tt[0]
		nameOut := tt[1]
		tag := p.NewTag(nameIn, "")
		is.Equal(tag.Name(), nameOut, "tag %q => %q", nameIn, nameOut)
	}
}
