package tag

import (
	"fmt"
)

// Exponential Moving Everage Weight Factor (0.1: new values have little effect, 0.9 new values have a large effect)
//
// The lower the number, the quicker we forget what came first
const emaWeight = 0.4

type stats struct {
	id          string   // tag id
	time        float64  // how often this tag appeared in the file group, max 1x per file
	positionEma float64  // the exponential moving average (ema) position of the tag in each tag list
	names       []string // slice of names used by this tag, most recent last
	colors      []string // slice of colors used by this tag, most recent last
}

func newStats(id string) *stats {
	return &stats{
		id:     id,
		names:  []string{""},
		colors: []string{NoColor},
	}
}

// Tick indicates that time has progress, so all statistics should be reduced
func (s *stats) Tick() {
	s.time++
	s.positionEma = (1 - emaWeight) * s.positionEma
}

func (s *stats) AddTag(tag *Tag, opts ...tagStatsOpt) {
	f := &statisticsFrame{}

	for _, o := range opts {
		o(f)
	}

	// ema0 = n
	// ema = ((1-w) * ema) + (w * n)

	// position ema: n = (listLength - position) => order descending
	p := float64(f.listLength - f.position)
	if s.positionEma == 0 {
		s.positionEma = p
	} else {
		s.positionEma += emaWeight * p
	}

	s.names = prependUnique(s.names, tag.name)

	// never overwrite a real color with none
	if tag.color != NoColor {
		s.colors = prependUnique(s.colors, tag.color)
	}
}

// prependUnique prepends a value to a list (by shuffling all existing values)
// while ensuring that the new value only appears once in the list
func prependUnique[T comparable](u []T, p T) []T {
	grow := true
	for i, n := range u {
		if p == n {
			grow = false
			break
		}
		u[i] = p
		p = n
	}
	if grow {
		u = append(u, p)
	}
	return u
}

type statisticsFrame struct {
	position   int
	listLength int
}

type tagStatsOpt func(*statisticsFrame)

func atPosition(p int) tagStatsOpt   { return func(f *statisticsFrame) { f.position = p } }
func ofListLength(l int) tagStatsOpt { return func(f *statisticsFrame) { f.listLength = l } }

func (s *stats) MergedTag() *Tag {
	return New(s.names[0], s.colors[0])
}

func (s *stats) String() string {
	return fmt.Sprintf("tag stats: %q pos: %.4f names: %+v colors: %+v",
		s.id,
		s.positionEma,
		s.names,
		s.colors,
	)
}

// tagStatsList type is only used for sorting tags based on their statistical
// priorities
type tagStatsList []*stats

func (l tagStatsList) Len() int      { return len(l) }
func (l tagStatsList) Swap(i, j int) { l[i], l[j] = l[j], l[i] }
func (l tagStatsList) Less(i, j int) bool {
	a := l[i]
	b := l[j]
	switch {
	case a.positionEma != b.positionEma:
		return a.positionEma > b.positionEma
	case a.id != b.id:
		return a.id < b.id
	default:
		return false
	}
}
