package tag

import "strings"

var isValidColor = map[string]bool{
	"none":   true,
	"red":    true,
	"orange": true,
	"yellow": true,
	"green":  true,
	"blue":   true,
	"purple": true,
	"grey":   true,
}

const NoColor = "none"

func ValidColorName(color string) string {
	color = strings.ToLower(color)
	switch _, ok := isValidColor[color]; ok {
	case true:
		return color
	default:
		return NoColor
	}
}

func (t *Tag) SetColor(color string) { t.color = ValidColorName(color) }
func (t *Tag) HasColor() bool        { return t.color != NoColor }
