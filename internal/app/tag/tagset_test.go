package tag_test

import (
	"strings"
	"testing"

	"codeberg.org/japh/is"
	"codeberg.org/japh/tix/internal/app/tag"
)

func TestTagMerging(t *testing.T) {
	is := is.New(t)

	// Notes:
	//  - each row represents the list of tags associated with a single file
	//  - each group id represents a unique hash - i.e. a group of duplicate files
	//  - rows are ordered chronologically
	//    i.e. from least recent / oldest to most recent / newest,
	//         according to each file's mtime timestamp, because xattrs do not have timestamps
	//    - more recent files are assumed to be "more important" than older files
	//    - the tag order of more recent file has higher priority than the order used by older files
	const (
		groupIdCol = iota
		tagsCol
		commentsCol
	)
	testGroups := is.TableFromString(`
        | group id               | tags  | comments                                      |
        | ====================== | ===== | ============================================= |
        | empty                  |       | no tags                                       |
        | empty                  |       |                                               |
        | empty                  |       |                                               |
        | ---------------------- | ----- | --------------------------------------------- |
        | single tag list        | c a b | only 1 set of tags, no change                 |
        | ---------------------- | ----- | --------------------------------------------- |
        | identical              | b a c |                                               |
        | identical              | b a c |                                               |
        | identical              | b a c |                                               |
        | ---------------------- | ----- | --------------------------------------------- |
        | empty full             |       |                                               |
        | empty full             |       |                                               |
        | empty full             | a b c | combine empty tag list with full tag set list |
        | ---------------------- | ----- | --------------------------------------------- |
        | empty full empty       |       |                                               |
        | empty full empty       | a b c |                                               |
        | empty full empty       |       |                                               |
        | ---------------------- | ----- | --------------------------------------------- |
        | full empty             | a b c |                                               |
        | full empty             |       |                                               |
        | full empty             |       | combine full tag list with empty tag list     |
        | ---------------------- | ----- | --------------------------------------------- |
        | recent first           | a     |                                               |
        | recent first           | b     | b is more recent, has higher priority than a  |
        | recent first           | c     | c is most recent, appears before b and a      |
        | ---------------------- | ----- | --------------------------------------------- |
        | simple moving average  | a b   |                                               |
        | simple moving average  | c a b |                                               |
        | simple moving average  | a     | a has the highest average                     |
        | ---------------------- | ----- | --------------------------------------------- |
        | prioritize newest      | a b c |                                               |
        | prioritize newest      | a b c |                                               |
        | prioritize newest      | c a b |                                               |
        | ---------------------- | ----- | --------------------------------------------- |
        | complex moving average | b a   |                                               |
        | complex moving average | c b a |                                               |
        | complex moving average | d a   |                                               |
        | complex moving average | b c a |                                               |
        | complex moving average | a b c | most recent == most important                 |
        `)

	// same columns as testGroups - no need for different consts
	testExpectations := is.TableFromString(`
        | group id               | tags                |
        | ---------------------- | ------------------- |
        | empty                  |                     |
        | single tag list        | @(c) @(a) @(b)      |
        | identical              | @(b) @(a) @(c)      |
        | empty full             | @(a) @(b) @(c)      |
        | empty full empty       | @(a) @(b) @(c)      |
        | full empty             | @(a) @(b) @(c)      |
        | recent first           | @(c) @(b) @(a)      |
        | simple moving average  | @(c) @(a) @(b)      |
        | prioritize newest      | @(a) @(c) @(b)      |
        | complex moving average | @(b) @(a) @(c) @(d) |
        `)

	tagSetForHash := map[string]*tag.Set{}

	// Note: DataRows does NOT contain the separator rows
	//       so we only want to skip the 1st row of headers => [1:]
	for _, tg := range testGroups.DataRows()[1:] {
		hash := tg[groupIdCol]
		tags := tg[tagsCol]

		// log.Println("--- hash:", hash)

		ts, ok := tagSetForHash[hash]
		if !ok {
			ts = tag.NewSet()
			tagSetForHash[hash] = ts
		}

		tl := tag.List{}
		for _, s := range strings.Fields(tags) {
			tl = append(tl, tagFromString(s))
		}

		ts.AddTagList(tl)
	}

	tested := map[string]bool{}
	for _, exp := range testExpectations.DataRows()[1:] {
		hash := exp[groupIdCol]
		tested[hash] = true

		// log.Println("--- hash:", hash)

		expTags := strings.Fields(exp[tagsCol])
		ts := tagSetForHash[hash]
		tl := ts.MergedTagList()

		gotJoined := strings.Join(tl.Strings(), ", ")
		wantJoined := strings.Join(expTags, ", ")
		is.Equal(gotJoined, wantJoined, "hash %q: tags [%s]", hash, wantJoined)

		ok := is.Equal(len(tl), len(expTags), "hash %q:   %d tags", hash, len(expTags))
		if ok {
			for i := 0; i < len(tl); i++ {
				is.Equal(tl[i].String(), expTags[i], "hash %q:   %d %q", hash, i, expTags[i])
			}
		}
	}

	for h := range tagSetForHash {
		if !tested[h] {
			t.Errorf("fail: hash %q was not tested", h)
		}
	}
}

func TestPreferredTags(t *testing.T) {
	is := is.New(t)

	const (
		haveSetCol = iota
		haveTagsCol
	)

	testTagSets := is.TableFromString(`
        | tag set | tags             |
        | ======= | ================ |
        | 1       | a b:red c        |
        | 1       | a b:green c:blue |
        | 1       | a B:yellow c     |
        `)

	const (
		prefsSetCol = iota
		prefsCol
		wantTagsCol
	)

	testPreferences := is.TableFromString(`
        | tag set | preferences | tags                                 |
        | ------- | ----------- | ------------------------------------ |
        | 1       |             | @(a) @(B)[yellow] @(c)[blue]         |
        | 1       | A:purple    | @(A)[purple] @(B)[yellow] @(c)[blue] |
        | 1       | b:none      | @(a) @(b) @(c)[blue]                 |
        `)

	tagSetForHash := map[string]*tag.Set{}

	// Note: DataRows does NOT contain the separator rows
	//       so we only want to skip the 1st row of headers => [1:]
	for _, tr := range testTagSets.DataRows()[1:] {
		hash := tr[haveSetCol]
		ts, ok := tagSetForHash[hash]
		if !ok {
			ts = tag.NewSet()
			tagSetForHash[hash] = ts
		}

		tl := tag.List{}
		for _, s := range strings.Fields(tr[haveTagsCol]) {
			tl = append(tl, tagFromString(s))
		}

		ts.AddTagList(tl) // tags must be added in chronological order
	}

	for _, exp := range testPreferences.DataRows()[1:] {
		hash := exp[prefsSetCol]
		prefs := strings.Fields(exp[prefsCol])
		wantTags := strings.Fields(exp[wantTagsCol])

		// log.Println("--- hash:", hash)
		ts := tagSetForHash[hash]

		ts.ClearPreferredTags()
		for _, p := range prefs {
			ts.PreferTag(tagFromString(p))
		}

		tl := ts.MergedTagList()
		ok := is.Equal(len(tl), len(wantTags), "hash %s: %d tags", hash, len(wantTags))
		if ok {
			for i := 0; i < len(tl); i++ {
				is.Equal(tl[i].String(), wantTags[i], "hash %s:   %d %q", hash, i, wantTags[i])
			}
		}
	}
}

func tagFromString(s string) *tag.Tag {
	p := strings.Split(s, ":")
	for len(p) != 2 {
		p = append(p, "")
	}
	return tag.New(p[0], p[1])
}
