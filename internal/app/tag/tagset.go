package tag

import "sort"

// Set collect multiple TagLists and provides methods for merging them into
// a single, common TagList which can be applied to all instances of a
// duplicated file.
type Set struct {
	tagLists        []List            // chronologically ordered list of TagLists (newer TagLists last)
	tagStatsForHash map[string]*stats // collator of a tag's preferred position, name and color
	preferredTag    map[string]*Tag   // user selected tag preference
}

// NewSet creates a new TagSet
func NewSet() *Set {
	return &Set{
		tagLists:        []List{},
		tagStatsForHash: map[string]*stats{},
		preferredTag:    map[string]*Tag{},
	}
}

// AddTagList adds an ordered list of tags to the set (for eventual merging)
func (ts *Set) AddTagList(tl List) {
	ts.tagLists = append(ts.tagLists, tl)

	for _, s := range ts.tagStatsForHash {
		s.Tick()
	}

	l := len(tl)
	for p, t := range tl {
		tid := t.id
		tst, ok := ts.tagStatsForHash[tid]
		if !ok {
			tst = newStats(tid)
			ts.tagStatsForHash[tid] = tst
		}
		tst.AddTag(t, atPosition(p), ofListLength(l))
	}
}

// PreferTag can be used to override the derived representation of a tag with a
// user-selected preference.
//
// Note that this overrides both the tag's name as well as its color
func (ts *Set) PreferTag(t *Tag) {
	ts.preferredTag[t.ID()] = t
}

// ClearPreferredTags removes all tag preferences, reverting to the most
// recently used name and color of each tag.
func (ts *Set) ClearPreferredTags() {
	ts.preferredTag = map[string]*Tag{}
}

// TagNames returns the full list of names that were used for a tag within this
// TagSet
func (ts *Set) TagNames(id string) []string {
	stats, ok := ts.tagStatsForHash[id]
	if !ok {
		return []string{""}
	}
	return stats.names
}

// TagColors returns the full list of colors that were used for a tag within
// this TagSet
func (ts *Set) TagColors(id string) []string {
	stats, ok := ts.tagStatsForHash[id]
	if !ok {
		return []string{NoColor}
	}
	return stats.colors
}

// TagLists returns the list of tagLists, in the order they were added to this TagSet
func (ts *Set) TagLists() []List {
	return ts.tagLists
}

// MergedTagList returns a TagList containing the full set of tags found in all
// TagLists within this TagSet, in an order that should keep the original order
// in tact as much as possible, while favouring later TagLists over earlier tag
// lists.
func (ts *Set) MergedTagList() List {
	tsl := make(tagStatsList, 0, len(ts.tagStatsForHash))
	for _, s := range ts.tagStatsForHash {
		tsl = append(tsl, s)
	}
	sort.Sort(tsl)

	mergedTags := make(List, 0, len(ts.tagStatsForHash))
	for _, s := range tsl {
		t, ok := ts.preferredTag[s.id] // the tag with the user-preferred name and color
		if !ok {
			t = s.MergedTag() // a tag with the most likely name and most likely color
		}
		if t != nil {
			mergedTags = append(mergedTags, t)
		}
	}

	return mergedTags
}
