package tag

// List represents the ordered list of tags for a single file
type List []*Tag

// NewTagList only provides a common New... interface to a simple list of tags
func NewTagList() List {
	return List{}
}

// Strings returns the list of tags as strings...
// only required to get around go's limited type casting
func (tl List) Strings() []string {
	s := make([]string, 0, len(tl))
	for _, t := range tl {
		s = append(s, t.String())
	}
	return s
}

func (this List) IsEqual(other List) bool {
	if len(this) != len(other) {
		return false
	}

	for i := range this {
		a := this[i]
		b := other[i]
		if a.Name() != b.Name() {
			return false
		}
		if a.Color() != b.Color() {
			return false
		}
	}

	return true
}
