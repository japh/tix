package apple_test

import (
	"testing"

	"codeberg.org/japh/is"
	"codeberg.org/japh/tix/internal/adapter/apple"
	"codeberg.org/japh/tix/internal/app/tag"
)

func TestAppleTagEncoding(t *testing.T) {
	is := is.New(t)

	// Given a tag list
	tl := tag.NewTagList()
	tl = append(tl, tag.New("one", ""))
	tl = append(tl, tag.New("two", "red"))
	tl = append(tl, tag.New("three", "green"))
	tl = append(tl, tag.New("four", ""))

	// When the tag list is encoded
	enc := &apple.Encoder{}
	plist, err := enc.Encode(tl)
	if err != nil {
		t.Logf("oops: %s", err.Error())
	}

	// And the encoded tag list is decoded again
	dec := &apple.Decoder{}
	dl, err := dec.Decode(plist)
	if err != nil {
		t.Logf("oops: %s", err.Error())
	}

	// Then the decoded tag list should be the same as the original tag list
	ok := is.Equal(len(dl), len(tl), "length of decoded list should be %d", len(tl))
	if ok {
		// And the tag colors should match the original tag colors
		// And the order of the tags should be the same
		for i := range tl {
			is.Equal(dl[i], tl[i], "  want: %q", tl[i])
		}
	}
}
