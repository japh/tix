package apple

import (
	"strings"

	"codeberg.org/japh/tix/internal/app/tag"
)

// see also pkg/app/tag/color.go

var colorNameForID = map[string]string{
	"0": "none",
	"1": "red",
	"2": "orange",
	"3": "yellow",
	"4": "green",
	"5": "blue",
	"6": "purple",
	"7": "grey",
}

var colorIDForName = map[string]string{
	"none":   "0",
	"red":    "1",
	"orange": "2",
	"yellow": "3",
	"green":  "4",
	"blue":   "5",
	"purple": "6",
	"grey":   "7",
}

func ColorIDForName(name string) string {
	id, ok := colorIDForName[strings.ToLower(name)]
	if ok {
		return id
	}
	return colorIDForName[tag.NoColor]
}

func ColorNameForID(colorID string) string {
	name, ok := colorNameForID[strings.ToLower(colorID)]
	if ok {
		return name
	}
	return tag.NoColor
}
