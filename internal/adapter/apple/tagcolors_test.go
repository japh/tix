package apple_test

import (
	"testing"

	"codeberg.org/japh/is"
	"codeberg.org/japh/tix/internal/adapter/apple"
)

func TestColorsFromAppleColorID(t *testing.T) {
	is := is.New(t)

	const (
		haveIDCol = iota
		wantNameCol
		commentsCol
	)
	tagColors := is.TableFromString(`
		| id | name   | comments                      |
		| -- | ------ | ----------------------------- |
		| 0  | none   |                               |
		| 1  | red    |                               |
		| 2  | orange |                               |
		| 3  | yellow |                               |
		| 4  | green  |                               |
		| 5  | blue   |                               |
		| 6  | purple |                               |
		| 7  | grey   |                               |
		| x  | none   | x is an invalid color => none |
		`)

	for _, tc := range tagColors.DataRows()[1:] {
		haveID := tc[haveIDCol]
		wantName := tc[wantNameCol]

		gotName := apple.ColorNameForID(haveID)
		is.Equal(gotName, wantName, "tag color name for Apple color id %q => %q", haveID, wantName)
	}
}

func TestAppleColorIDsForColors(t *testing.T) {
	is := is.New(t)

	const (
		haveNameCol = iota
		wantIDCol
		commentsCol
	)
	tagColors := is.TableFromString(`
		| name     | id | comments                    |
		| -------- | -- | --------------------------- |
		| none     | 0  |                             |
		| red      | 1  |                             |
		| orange   | 2  |                             |
		| Yellow   | 3  | colors are case sensitive   |
		| GREEN    | 4  | -- " --                     |
		| bLUe     | 5  | -- " --                     |
		| purple   | 6  |                             |
		| grey     | 7  |                             |
		| HOT pink | 0  | invalid colors default to 0 |
		`)

	for _, tc := range tagColors.DataRows()[1:] {
		haveName := tc[haveNameCol]
		wantID := tc[wantIDCol]

		gotID := apple.ColorIDForName(haveName)
		is.Equal(gotID, wantID, "apple color id for color name %q => %q", haveName, wantID)
	}
}
