//
// Translation between Apple's tag colors to simple color names used by tix
//
//	- Apple stores tag colors as a single-digit number between 0 and 7
//	- the RGB colors here were 'captured' manually
//	  - I strongly expect these specific colors to change over time
//	  - tix clients should interperet the colors as they see fit
//	- tix ONLY uses the color names
//
// | id | name   | background | border | comment            |
// | -- | ------ | ---------- | ------ | ------------------ |
// | 0  | none   | 0000       | 0000   | transparent (RGBA) |
// | 1  | red    | EF7163     | EB4E3E |                    |
// | 2  | orange | F4AE60     | F19D3B |                    |
// | 3  | yellow | F9D86B     | F29F41 |                    |
// | 4  | green  | 83D577     | 63C956 |                    |
// | 5  | blue   | 5D93F9     | 3578F7 |                    |
// | 6  | purple | B679DF     | A357D7 |                    |
// | 7  | grey   | A5A4A8     | 98979C |                    |
//
// Technical Reference:
//
//	https://eclecticlight.co/2017/12/27/xattr-com-apple-metadata_kmditemusertags-finder-tags/
//
package apple

import (
	"fmt"
	"strings"

	"github.com/pkg/xattr"

	"codeberg.org/japh/tix/internal/app/tag"
	"howett.net/plist"
)

const xattrTagsPropertyName = "com.apple.metadata:_kMDItemUserTags"

// Tag Retrieval

func Tags(path string) (tag.List, error) {
	bplist, err := xattr.Get(path, xattrTagsPropertyName)
	if err != nil {
		return tag.List{}, nil // don't care
	}

	dec := &Decoder{}
	tags, err := dec.Decode(bplist)
	if err != nil {
		return nil, err
	}

	return tags, nil
}

type Decoder struct {
}

func (d *Decoder) Decode(bplist []byte) (tag.List, error) {

	tagStrings := []string{}

	_, err := plist.Unmarshal(bplist, &tagStrings)
	if err != nil {
		return nil, err
	}

	tags := tag.NewTagList()
	for _, tStr := range tagStrings {
		tParts := strings.Split(tStr, "\n")
		name := ""
		color := ""
		if len(tParts) >= 1 {
			name = tParts[0]
		}
		if len(tParts) >= 2 {
			color = ColorNameForID(tParts[1])
		}
		t := tag.New(name, color)
		if t != nil {
			tags = append(tags, t)
		}
	}

	return tags, nil
}

// Tag Storage

func SetTags(path string, tags tag.List) error {
	enc := &Encoder{}
	bplist, err := enc.Encode(tags)
	if err != nil {
		return err
	}

	err = xattr.Set(path, xattrTagsPropertyName, bplist)
	if err != nil {
		return err
	}

	return nil
}

type Encoder struct {
}

func (e *Encoder) Encode(tl tag.List) ([]byte, error) {
	// Apple's tag list is simply an array of strings
	// each string contains a \n separating the

	sl := make([]string, len(tl))
	for i, t := range tl {
		switch t.HasColor() {
		case true:
			sl[i] = fmt.Sprintf("%s\n%s", t.Name(), ColorIDForName(t.Color()))
		default:
			sl[i] = t.Name()
		}
	}

	b, err := plist.Marshal(sl, plist.BinaryFormat)
	if err != nil {
		return nil, err
	}

	return b, nil
}
