package hash

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"hash"

	"golang.org/x/crypto/sha3"
)

type Hasher interface {
	Algorithm() string
	Write([]byte) (int, error)
	HashString() string
}

type HasherBuilder func() Hasher

var hashers map[string]HasherBuilder

func init() {
	hashers = map[string]HasherBuilder{
		"md5":      func() Hasher { return &shaHash{"md5", md5.New()} },
		"sha1":     func() Hasher { return &shaHash{"sha1", sha1.New()} },
		"sha256":   func() Hasher { return &shaHash{"sha256", sha256.New()} },
		"shake256": func() Hasher { return &shakeHash{"shake256", sha3.NewShake256()} },
	}
}

func New(algo string) HasherBuilder {
	hb, ok := hashers[algo]
	if !ok {
		panic(fmt.Sprintf("no such hash algorithm '%s'", algo))
	}
	return hb
}

type Algo string

// Algorithm returns the name of the algorithm provided by this
// hasher object
func (a Algo) Algorithm() string { return string(a) }

// shaHash wraps normal hash.Hash objects
type shaHash struct {
	Algo      // provides Algorithm()
	hash.Hash // provides Write()
}

// HashString returns the hash as a hexadecimal string
func (h *shaHash) HashString() string {
	return hex.EncodeToString(h.Hash.Sum(nil))
}

// shakeHash wraps sha3's ShakeHash objects - which require a different API from hash.Hash
type shakeHash struct {
	Algo           // provides Algorithm()
	sha3.ShakeHash // provides Write()
}

// HashString returns the hash as a hexadecimal string
func (h *shakeHash) HashString() string {
	b := make([]byte, 32)
	h.ShakeHash.Read(b)
	return hex.EncodeToString(b)
}
