package hash_test

import (
	"testing"

	"codeberg.org/japh/is"
	"codeberg.org/japh/tix/internal/adapter/hash"
)

func TestHasher_unknown_algo(t *testing.T) {
	is := is.New(t)

	const (
		inputCol = iota
		algoCol
		digestCol
	)
	testCases := is.TableFromString(`
		| input   | algo     | digestCol                                                        |
		| ------- | -------- | ---------------------------------------------------------------- |
		|         | md5      | d41d8cd98f00b204e9800998ecf8427e                                 |
		|         | sha1     | da39a3ee5e6b4b0d3255bfef95601890afd80709                         |
		|         | sha256   | e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855 |
		|         | shake256 | 46b9dd2b0ba88d13233b3feb743eeb243fcd52ea62b81b82b50c27646ed5762f |
		| hash me | md5      | 17b31dce96b9d6c6d0a6ba95f47796fb                                 |
		| hash me | sha1     | 43f932e4f7c6ecd136a695b7008694bb69d517bd                         |
		| hash me | sha256   | eb201af5aaf0d60629d3d2a61e466cfc0fedb517add831ecac5235e1daa963d6 |
		| hash me | shake256 | 942e9f23a5e4229b8fab1b313e8faae59e883a0911dd71a567670f688e365d3b |
		`)

	for _, tc := range testCases.DataRows()[1:] {
		input := tc[inputCol]
		algo := tc[algoCol]
		wantDigest := tc[digestCol]

		hb := hash.New(algo)
		h := hb()
		h.Write([]byte(input))
		gotAlgo := h.Algorithm()
		gotDigest := h.HashString()
		is.Equal(gotAlgo, algo, "hash algorithm")
		is.Equal(gotDigest, wantDigest, "%s digest for %q", algo, input)
	}
}
