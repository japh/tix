package fs

import (
	"fmt"
	"io/fs"
	"path/filepath"
	"strings"
	"time"

	"codeberg.org/japh/id64"
	"codeberg.org/japh/tix/internal/app/progress"
	"codeberg.org/japh/tix/internal/app/tag"
)

type walker struct {
	bar   *progress.Bar
	store fileStore
}

type fileStore interface {
	Begin() error
	RegisterItem(path string, size int64, mTime time.Time)
	RegisterItemTags(itemID id64.ID64, tags tag.List)
	Commit() error
}

// fs.Walker walks all files and directories looking for files to be indexed.
// The list of files found is stored in the fileStore for further processing.
func Walker(store fileStore) *walker {
	return &walker{
		store: store,
	}
}

// Walk traverses one or more directories recursively, looking for any regular files
// TODO: 2023-01-05 remove dependency on progressBar
func (w *walker) Walk(root string, bar *progress.Bar) error {
	w.bar = bar

	if err := w.store.Begin(); err != nil {
		return err
	}

	filepath.WalkDir(root, w.step)

	if err := w.store.Commit(); err != nil {
		return err
	}

	return nil
}

func (w *walker) step(path string, d fs.DirEntry, err error) error {

	if d == nil {
		return fmt.Errorf("cannot walk into directory %s\n%w", path, err)
	}

	t := d.Type()
	i, err := d.Info()
	if err != nil {
		return fmt.Errorf("cannot get info for directory %s\n%w", path, err)
	}

	n := i.Name()

	switch {

	case n == "." || n == "..":
		return nil
		// silently skip all ./ and ../ directory items

	case strings.HasPrefix(n, "."):
		if i.IsDir() {
			// don't recurse into hidden directories
			// note: returning fs.SkipDir skips the parent directory if path is not a directory
			return fs.SkipDir
		}
		// silently ignore hidden files
		return nil

	case !t.IsRegular():
		return nil
	}

	w.store.RegisterItem(path, i.Size(), i.ModTime())
	w.bar.Did(1) // update the progress meter

	return nil
}
