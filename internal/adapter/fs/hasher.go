package fs

import (
	"io"
	"log"
	"os"

	"codeberg.org/japh/tix/internal/adapter/hash"
)

type fileHasher struct {
	hash.HasherBuilder
}

func New(hb hash.HasherBuilder) *fileHasher {
	return &fileHasher{HasherBuilder: hb}
}

func (fh *fileHasher) Algorithm() string {
	return fh.HasherBuilder().Algorithm()
}

func (fh *fileHasher) HashFile(path string) string {
	// fmt.Printf("\rhashing %s ...", path)

	f, err := os.Open(path)
	if err != nil {
		log.Println(err)
		return ""
	}
	defer f.Close()

	hb := fh.HasherBuilder()
	if _, err := io.Copy(hb, f); err != nil {
		log.Println()
		log.Fatal(err)
	}

	return hb.HashString()
}
