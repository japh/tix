package sqlite

import (
	"database/sql"
	"log"
	"os"
	"path/filepath"
	"strings"

	schema "codeberg.org/japh/go-sqlite-schema"
	_ "github.com/mattn/go-sqlite3"
)

type store struct {
	rootDir string // directory being indexed
	dbFile  string // name of database file (no path)
	dbPath  string // actual path of dbFile in rootDir or parent thereof
	db      *sql.DB
	tx      *sql.Tx
}

var openPragmas = []string{
	"_foreign_keys=on",
	"_journal_mode=wal",
}

func New(opts ...OptFunc) *store {
	s := &store{}

	for _, o := range opts {
		o(s)
	}

	// find the closest existing tix database
	// ... or default to the directory provided
	dbDir := s.rootDir
	s.dbPath = filepath.Join(dbDir, s.dbFile)
	for {
		dbPath := filepath.Join(dbDir, s.dbFile)

		info, err := os.Stat(dbPath)
		if err == nil && info.Mode().IsRegular() {
			s.dbPath = dbPath
			break
		}

		dbDir = filepath.Dir(dbDir)
		if dbDir == "/" {
			break
		}
	}

	log.Printf("root:  %q", s.rootDir)
	log.Printf("index: %q", s.dbPath)
	dsn := s.dbPath + "?" + strings.Join(openPragmas, "&")
	db, err := sql.Open("sqlite3", dsn)
	if err != nil {
		panic(err)
	}

	// make sure the database is up-to-date
	err = schema.Upgrade(db, schemaUpgradeScripts())
	if err != nil {
		panic(err)
	}

	s.db = db

	return s
}

// Close releases any resources held by the store
func (s *store) Close() {
	s.vacuum()
	s.db.Close()
}

type OptFunc func(*store)

func WithRootDir(dirName string) OptFunc {
	return func(s *store) {
		s.rootDir = dirName
	}
}

func WithDBFile(fileName string) OptFunc {
	return func(s *store) {
		s.dbFile = fileName
	}
}
