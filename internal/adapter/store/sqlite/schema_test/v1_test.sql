pragma user_version = 1;

insert into file_info ( path, updated, size, algo, hash )
values ( '/test/file-1', '2024-01-06 12:19:31', 1234, null,   null           ),
       ( '/test/file-2', '2024-01-06 12:19:31', 2345, 'sha1', 'abcdef012345' );
