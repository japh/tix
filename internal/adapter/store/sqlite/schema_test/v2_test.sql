pragma user_version = 2;

insert into file_info ( path,           updated,               size, algo,   hash           )
               values ( '/test/file-1', '2024-01-06 12:19:31', 1234, null,   null           ),
                      ( '/test/file-2', '2024-01-06 12:19:31', 2345, 'sha1', 'abcdef012345' );

insert into file_tags ( path,           tag,             pos, color )
               values ( '/test/file-2', 'Uncolored-tag', 0,   0     ),
                      ( '/test/file-2', 'Grey-tag',      1,   1     ),
                      ( '/test/file-2', 'Green-tag',     2,   2     ),
                      ( '/test/file-2', 'Purple-tag',    3,   3     ),
                      ( '/test/file-2', 'Blue-tag',      4,   4     ),
                      ( '/test/file-2', 'Yellow-tag',    5,   5     ),
                      ( '/test/file-2', 'Red-tag',       6,   6     ),
                      ( '/test/file-2', 'Orange-tag',    7,   7     );

