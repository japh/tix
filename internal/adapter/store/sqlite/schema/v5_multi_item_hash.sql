begin transaction;

--
-- New table structure
--

drop index if exists item_hash_idx;
drop view if exists duplicate_items;

create table item_hash (
    item_id text not null,
    algo    text not null,
    hash    text not null,
    --
    primary key ( item_id, algo ),
    foreign key ( item_id ) references item ( item_id ) on delete cascade
);

create view duplicate_items as
with
distinct_hashes as (
    select
        algo     as algo,
        hash     as hash,
        count(*) as count
    from
        item_hash
    group by
        algo,
        hash
    having count(*) > 1
)
select
    d.count     as count,
    d.algo      as algo,
    d.hash      as hash,
    i.size      as size,
    i.item_id   as item_id,
    i.path      as path
from
         distinct_hashes d
    join item_hash       h  on h.algo    = d.algo
                           and h.hash    = d.hash
    join item            i  on i.item_id = h.item_id
;

insert into item_hash ( item_id, algo, hash )
select
    item_id                               as item_id,
    algo                                  as algo,
    substr( hash, instr( hash, ':' ) +1 ) as hash   -- strip old algo prefix
from
    item
where
        algo is not null
    and hash is not null
;

create index item_hash_idx on item_hash ( hash, algo );

create trigger item_size_trg
after update of size on item
begin
    delete from item_hash where item_id = old.item_id;
end;

alter table item drop column algo;
alter table item drop column hash;

--
-- update schema version info
--
pragma user_version = 5;

commit transaction;
