begin transaction;

create table file_tags (
    path    text    not null,   -- partial path name of the file - must be unique
    tag     text    not null,   -- the text of a tag assigned to this file
    pos     integer not null,   -- the position of the tag in the files list of tags
    color   integer,            -- see https://eclecticlight.co/2017/12/27/xattr-com-apple-metadata_kmditemusertags-finder-tags/
    primary key ( path, tag ),
    foreign key ( path ) references file_info ( path ) on delete cascade
);

--
-- update schema version info
--
pragma user_version = 2;

commit transaction;
