begin transaction;

--
-- New table structure
--

alter table item add column vtime datetime; -- time of most recent re-validation

--
-- update schema version info
--
pragma user_version = 4;

commit transaction;
