begin transaction;

-- Assumptions:
--
--  items (i.e. images or videos) are stored in a separate directory for each item
--  the directory structure is:
--      <id>/
--          info.json
--          <variant>.<format>
--
--  The info.json file should reflect the data stored in this database and can
--  be used to re-construct the database if needed.
--  e.g.
--      {
--          "id": "abcDEF01234",
--          "name": ...,
--          "created": ...,
--          "tags": [ "one", "two\n3", "three" ],
--          "variants": {
--              "thumb":  { "url": "abcDEF01234-thumb.jpg",   "type": "image/jpeg", "size": 1234    },
--              "vthumb": { "url": "abcDEF01234-vthumb.jpg",  "type": "image/jpeg", "size": 12345   },
--              "poster": { "url": "abcDEF01234-poster.png",  "type": "image/png",  "size": 2345    },
--              "sd":     { "url": "abcDEF01234-mobile.webm", "type": "video/webm", "size": 123456  },
--              "hd":     { "url": "abcDEF01234.mp4",         "type": "video/mp4",  "size": 1234567 },
--          }
--      }

--
-- New table structure
--

create table item (
    item_id text    not null,   -- 64bit random id assigned at first contact (similar to youtube video id's)
    path    text,               -- full path of the original file provided
    name    text,               -- name of the original file provided, without path or format suffix
    ctime   datetime,           -- most recently captured ctime of the file, will change on when inode is modified
    mtime   datetime,           -- most recently captured mtime of the file, will change when data is modified
    size    integer,            -- most recently captured size of the file
    algo    text,               -- hash algorithm name
    hash    text,               -- hash build from the file's content
    --
    primary key ( item_id ),
    unique      ( path )        -- prevent the same path being registered twice
);

-- create a non-unique index to allow duplicate files, but also make it easy to find them
create index item_hash_idx on item ( hash, algo );

create table item_tag (
    item_id text    not null,                   -- id of an item
    pos     integer not null,                   -- the position of the tag in the files list of tags
    tag_id  text    not null,                   -- lower-case tag name
    name    text    not null,                   -- the text of a tag assigned to this file
    color   text    not null default 'none',    -- explicit name of color - independent of apple's 'rainbow' numbering scheme
    --
    primary key ( item_id, pos    ),
    unique      ( item_id, tag_id ),
    foreign key ( item_id ) references item ( item_id ) on delete cascade
);

create table item_variant (
    item_id text    not null,   -- id of an item
    variant text    not null,   -- e.g. thumb, vthumb, poster, sd, hd
    url     text    not null,   -- e.g. abcDEF01234-thumb.jpg
    format  text    not null,   -- e.g. image/jpeg, video/mp4, video/webm
    size    integer not null,   -- size of this variant
    --
    primary key ( item_id, variant ),
    foreign key ( item_id ) references item ( item_id ) on delete cascade
);

create view duplicate_items as
with
dups as ( select hash, count(*) as count from item group by hash having count(*) > 1 )
select
    d.count     as count,
    d.hash      as hash,
    i.size      as size,
    i.item_id   as item_id,
    i.path      as path
from
         dups d
    join item i on i.hash = d.hash
;

--
-- Migrate existing data to new table structure
--

-- Note: SQLite does not have regex built in by default, so there is no way to
-- reduce paths to (base)names - we leave the name column set to NULL so that
-- the application can detect which names need to be cleaned up later
insert into item ( item_id, path, mtime, size, algo, hash)
select
    substr( format( '-%x', random() ), 1, 10 ) as item_id,  -- poor man's valid id64
    path                                       as path,
    updated                                    as mtime,
    size                                       as size,
    algo                                       as algo,
    hash                                       as hash
from
    file_info
;

insert into item_tag ( item_id, pos, tag_id, name, color )
with
apple_color( color_id, name ) as (
    -- see https://eclecticlight.co/2017/12/27/xattr-com-apple-metadata_kmditemusertags-finder-tags/
    values ( 0, 'none'   ),
           ( 1, 'grey'   ),
           ( 2, 'green'  ),
           ( 3, 'purple' ),
           ( 4, 'blue'   ),
           ( 5, 'yellow' ),
           ( 6, 'red'    ),
           ( 7, 'orange' )
)
select
    i.item_id      as item_id,
    t.pos          as pos,
    lower( t.tag ) as tag_id,
    t.tag          as name,
    c.name         as color
from
              file_tags   t
         join item        i on i.path     = t.path
    left join apple_color c on c.color_id = t.color
;

--
-- Remove old tables
--

-- Note: drop the dependent tables first - less cascading overhead
drop table if exists file_tags;
drop table if exists file_info;

--
-- update schema version info
--
pragma user_version = 3;

commit transaction;
