begin transaction;

-- tix cache of file information
--
-- Assumptions:
--
--  files are unlikely to change without...
--      - altering their ctime (updated) timestamp
--      - altering their size
--
--  file renaming is not an issue
--      - but every file's path must be unique
--      - deleting a file and renaming another file to takes its place is
--        treated like a simple update to the existing file
--
--  files with a unique size cannot have any duplicates
--  hashing files to ensure uniqueness is only required if >1 files are
--  detected with the same size.
--      => most files will not need to be hashed!
--
create table file_info (
    path    text     not null,  -- partial path name of the file - must be unique
    updated datetime,           -- most recent ctime of the file
    size    integer,            -- most recent size of the file
    algo    text,               -- hash algorithm name
    hash    text,               -- hash build from the file's content
    primary key ( path )
);

--
-- update schema version info
--
pragma user_version = 1;

commit transaction;
