package sqlite

import (
	"embed"
	"log"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
)

//go:embed schema/v*.sql
var embeddedFS embed.FS

// schemaUpgradeScripts returns a list of SQL statements used to migrate a database
// schema from any historical version to the most recent structure.
//
// see codeberg.org/japh/go-sqlite-schema
//
func schemaUpgradeScripts() []string {

	sqlFileNames := sortedScriptNames(embeddedFileNames())

	sqlScripts := []string{}
	for _, sqlFile := range sqlFileNames {
		sql, err := embeddedFS.ReadFile(sqlFile)
		if err != nil {
			log.Fatalf("failed to read embedded file %s\n%s", sqlFile, err)
		}
		sqlScripts = append(sqlScripts, string(sql))
	}

	return sqlScripts
}

func embeddedFileNames() []string {
	schemaDir := "schema"
	dirItems, err := embeddedFS.ReadDir(schemaDir) // from embedded FS
	if err != nil {
		log.Fatalf("failed to read filenames from embedded filesystem")
	}

	fileNames := make([]string, 0, len(dirItems))
	for _, i := range dirItems {
		fileNames = append(fileNames, filepath.Join(schemaDir, i.Name()))
	}

	return fileNames
}

// sortedScriptNames filters and sorts a list of file names numerically, suitable for
// being applied to a database in sequence.
//
// A fatal error will be logged if the resulting list of sql scripts are not
// numbered sequentially from 1 upwards
func sortedScriptNames(fileNames []string) []string {
	// only want schema upgrade scripts
	// e.g. v1.sql, v002_improved.sql, etc.
	upgradeScriptRE := regexp.MustCompile(`^(?:.*/)?v(\d+).*\.sql$`)

	type scriptInfo struct {
		name    string
		version int
	}

	// extract the name and schema version of each file
	// - skip any files which do not look like an upgrade script
	scripts := []scriptInfo{}
	for _, n := range fileNames {
		match := upgradeScriptRE.FindStringSubmatch(n)
		if len(match) == 0 {
			continue
		}

		version, err := strconv.Atoi(match[1])
		if err != nil {
			continue
		}

		script := scriptInfo{
			name:    match[0],
			version: version,
		}

		scripts = append(scripts, script)
	}

	// sort scripts numerically by version number (e.g. 10 > 2)
	sort.Slice(scripts, func(a, b int) bool {
		return scripts[a].version < scripts[b].version
	})

	// ensure that all scripts are numbered sequentially from 1 upwards
	scriptNames := []string{}
	for i, s := range scripts {
		if i != s.version-1 {
			log.Printf("unexpected schema script #%d: %s", i, s.name)
			log.Fatalf("non-sequentially numbered schema versions detected")
		}
		scriptNames = append(scriptNames, s.name)
	}

	return scriptNames
}
