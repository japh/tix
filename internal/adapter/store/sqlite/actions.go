package sqlite

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"time"

	"codeberg.org/japh/id64"
	"codeberg.org/japh/tix/internal/app"
	"codeberg.org/japh/tix/internal/app/tag"
)

func (s *store) Begin() error {
	tx, err := s.db.Begin()
	if err != nil {
		return err
	}
	s.tx = tx
	return nil
}

func (s *store) Commit() error {
	err := s.tx.Commit()
	if err != nil {
		return err
	}
	s.tx = nil
	return nil
}

// vacuum is only called implicitly via store.Close()
func (s *store) vacuum() error {
	_, err := s.db.Exec(`vacuum`)
	if err != nil {
		return err
	}
	return nil
}

func (s *store) AllItems() []*app.Item {

	itemMap := map[id64.ID64]*app.Item{}

	s.loadAllItems(itemMap)
	s.loadAllHashes(itemMap)
	s.loadAllTags(itemMap)

	items := make([]*app.Item, 0, len(itemMap))
	for _, i := range itemMap {
		items = append(items, i)
	}

	return items
}

func (s *store) loadAllItems(itemMap map[id64.ID64]*app.Item) {
	query := `select item_id, path, name, size, mtime from item`
	rows, err := s.db.QueryContext(context.Background(), query)
	if err != nil {
		panic(fmt.Sprintf("failed to get all items meta-data: %q", err))
	}
	defer rows.Close()

	for rows.Next() {
		c := struct {
			itemID sql.NullString
			path   sql.NullString
			name   sql.NullString
			size   sql.NullInt64
			mtime  sql.NullTime
			ctime  sql.NullTime
		}{}
		err := rows.Scan(&c.itemID, &c.path, &c.name, &c.size, &c.mtime)
		if err != nil {
			panic(fmt.Sprintf("failed to scan a row of file info: %q", err))
		}

		itemID, _ := id64.FromString(c.itemID.String)

		i := app.NewItem()
		if itemID != 0 {
			i.ItemID = itemID
		}
		i.Path = c.path.String
		i.Name = c.name.String
		i.Size = c.size.Int64
		i.MTime = c.mtime.Time
		i.CTime = c.ctime.Time

		itemMap[i.ItemID] = i
	}

}

func (s *store) loadAllHashes(itemMap map[id64.ID64]*app.Item) {
	query := `select item_id, algo, hash from item_hash order by item_id`
	rows, err := s.db.QueryContext(context.Background(), query)
	if err != nil {
		panic(fmt.Sprintf("failed to get all hashes: %q", err))
	}
	defer rows.Close()

	for rows.Next() {
		c := struct {
			itemID sql.NullString
			algo   sql.NullString
			hash   sql.NullString
		}{}
		err := rows.Scan(&c.itemID, &c.algo, &c.hash)
		if err != nil {
			panic(fmt.Sprintf("failed to scan a row of file tags: %q", err))
		}

		itemID, _ := id64.FromString(c.itemID.String)
		i, ok := itemMap[itemID]
		if !ok {
			continue
		}

		algo := c.algo.String
		hash := c.hash.String
		i.Hash[algo] = hash
	}
}

func (s *store) loadAllTags(itemMap map[id64.ID64]*app.Item) {
	query := `select item_id, name, color from item_tag order by item_id, pos`
	rows, err := s.db.QueryContext(context.Background(), query)
	if err != nil {
		panic(fmt.Sprintf("failed to get all tags: %q", err))
	}
	defer rows.Close()

	for rows.Next() {
		c := struct {
			itemID sql.NullString
			name   sql.NullString
			color  sql.NullString
		}{}
		err := rows.Scan(&c.itemID, &c.name, &c.color)
		if err != nil {
			panic(fmt.Sprintf("failed to scan a row of file tags: %q", err))
		}

		itemID, _ := id64.FromString(c.itemID.String)
		f, ok := itemMap[itemID]
		if !ok {
			continue
		}

		f.Tags = append(f.Tags, tag.New(c.name.String, c.color.String))
	}
}

func (s *store) RegisterItem(path string, size int64, mTime time.Time) {
	// log.Printf("inserting item %v (%v:%v)", path, size, mTime)

	id := id64.RandomID()
	slash := strings.LastIndex(path, "/")
	name := path[slash+1:]

	query := `insert into item (  item_id,  path,  name,  size,  mtime,  vtime )
                        values ( :item_id, :path, :name, :size, :mtime, :vtime )
              on conflict( path ) do
                  update set
                      name  = excluded.name,
                      size  = excluded.size,
                      mtime = excluded.mtime,
                      vtime = excluded.vtime
                  where
                        excluded.size  != item.size
                    or  excluded.mtime != item.mtime
                `

	args := []any{
		sql.Named("item_id", id.String()),
		sql.Named("path", path),
		sql.Named("name", name),
		sql.Named("size", size),
		sql.Named("mtime", mTime.UTC().Format(time.RFC3339)),
		sql.Named("vtime", time.Now().UTC().Format(time.RFC3339)),
		// sql.Named("ctime", cTime.UTC().Format(time.RFC3339)),    // cTime not available
	}

	_, err := s.db.ExecContext(context.Background(), query, args...)
	if err != nil {
		panic(fmt.Sprintf("failed insert: %s", err))
	}

	return
}

func (s *store) RegisterItemTags(itemID id64.ID64, tags tag.List) {

	_, err := s.db.ExecContext(context.Background(), `delete from item_tag where item_id = :item_id`, sql.Named("item_id", itemID.String()))
	if err != nil {
		panic(fmt.Sprintf("failed to clear tags for %q\n%s", itemID, err))
	}

	query := `insert into item_tag (  item_id,  pos,  tag_id,  name,  color )
                            values ( :item_id, :pos, :tag_id, :name, :color )
              on conflict do
                  update set
                      pos    = excluded.pos,
                      tag_id = excluded.tag_id,
                      name   = excluded.name,
                      color  = excluded.color
              `

	for pos, tag := range tags {
		args := []any{
			sql.Named("item_id", itemID.String()),
			sql.Named("pos", pos),
			sql.Named("tag_id", tag.ID()),
			sql.Named("name", tag.Name()),
			sql.Named("color", tag.Color()),
		}

		_, err := s.db.ExecContext(context.Background(), query, args...)
		if err != nil {
			panic(fmt.Sprintf("failed tag update: %s", err))
		}
	}

	_, err = s.db.ExecContext(context.Background(), `update item set vtime = :vtime where item_id = :item_id`,
		sql.Named("item_id", itemID.String()),
		sql.Named("vtime", time.Now().UTC().Format(time.RFC3339)),
	)
	if err != nil {
		panic(fmt.Sprintf("failed to update verification time of item %q\n%s", itemID.String(), err))
	}

	return
}

func (s *store) RegisterItemHash(itemID id64.ID64, algo, hash string) {

	// log.Printf("updating hash %v (%v:%v)", path, algo, hash)

	query := `
             insert into item_hash (  item_id,  algo,  hash )
                            values ( :item_id, :algo, :hash )
             on conflict( item_id, algo ) do
                 update set
                     algo = :algo,
                     hash = :hash
              `

	args := []any{
		sql.Named("item_id", itemID.String()),
		sql.Named("algo", algo),
		sql.Named("hash", hash),
	}

	_, err := s.db.ExecContext(context.Background(), query, args...)
	if err != nil {
		panic(fmt.Sprintf("failed %s hash update for %s: %s", algo, itemID, err))
	}

	return
}
