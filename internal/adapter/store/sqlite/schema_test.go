package sqlite

import (
	"fmt"
	"testing"

	"codeberg.org/japh/is"
)

// Given an empty file list
//  When the file list is filtered and sorted
//  Then the file list should be empty
func TestEmptyFileList(t *testing.T) {
	is := is.New(t)

	givenFiles := []string{}
	wantFiles := []string{}

	gotFiles := sortedScriptNames(givenFiles)

	is.Equal(gotFiles, wantFiles, "empty file list")
}

// Given a file list
//   And the file list contains schema scripts
//   And the file list contains non-schema files
//  When the file list is filtered and sorted
//  Then the file list should only contain schema scripts
func TestSchemaScriptFilter(t *testing.T) {
	is := is.New(t)

	givenFiles := []string{
		".",
		"..",
		"ReadMe.md",
		"v1.sql",
	}

	wantFiles := []string{
		"v1.sql",
	}

	gotFiles := sortedScriptNames(givenFiles)

	is.Equal(gotFiles, wantFiles, "file list with schema upgrade scripts only")
}

// Given a file list
//   And the file list contains schema scripts
//   And the file list is unsorted
//  When the file list is filtered and sorted
//  Then the file list should be sorted numerically
func TestNumericalSorting(t *testing.T) {
	is := is.New(t)

	givenFiles := []string{}
	wantFiles := []string{}

	maxVersion := 30
	for v := 1; v < maxVersion; v++ {
		givenFiles = append(givenFiles, fmt.Sprintf("v%d.sql", maxVersion-v))
		wantFiles = append(wantFiles, fmt.Sprintf("v%d.sql", v))
	}

	gotFiles := sortedScriptNames(givenFiles)

	is.Equal(gotFiles, wantFiles, "scripts sorted numerically")
}

// Given a file list
//   And the file list contains schema scripts
//   And some files are numbered with leading 0's
//  When the file list is filtered and sorted
//  Then the file list should be sorted numerically
func TestLeadingZeroSorting(t *testing.T) {
	is := is.New(t)

	givenFiles := []string{
		"v003.sql",
		"v02.sql",
		"v1.sql",
	}

	wantFiles := []string{
		"v1.sql",
		"v02.sql",
		"v003.sql",
	}

	gotFiles := sortedScriptNames(givenFiles)

	is.Equal(gotFiles, wantFiles, "numerical sorting with optional leading 0's")
}

// Given a file list
//   And the file list contains schema scripts
//   And some files have additional text after their version number
//  When the file list is filtered and sorted
//  Then the file list should be sorted numerically
func TestFilesWithDescriptiveNames(t *testing.T) {
	is := is.New(t)

	givenFiles := []string{
		"v02_user_table_added.sql",
		"v1_init_version.sql",
		"v3_configuration_table_added.sql",
	}

	wantFiles := []string{
		"v1_init_version.sql",
		"v02_user_table_added.sql",
		"v3_configuration_table_added.sql",
	}

	gotFiles := sortedScriptNames(givenFiles)

	is.Equal(gotFiles, wantFiles, "schema scripts with descriptive names")
}
