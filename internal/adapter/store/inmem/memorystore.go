package inmem

import (
	"strings"
	"time"

	"codeberg.org/japh/tix/internal/app"
	"codeberg.org/japh/tix/internal/app/tag"
)

type store struct {
	fileWithPath map[string]*app.Item
}

// New creates a new file store object
func New(opts ...optFunc) *store {
	return &store{
		fileWithPath: map[string]*app.Item{},
	}
}

type optFunc func(*store)

// dummy to match database stores
func WithRootDir(dir string) optFunc {
	return func(s *store) {}
}
func WithDBFile(file string) optFunc {
	return func(s *store) {}
}

// Begin & Commit do nothing for in-memory stores
func (s *store) Begin() error  { return nil }
func (s *store) Commit() error { return nil }

// AllItems returns a slice of all tix.FileInfo structs
func (s *store) AllItems() []*app.Item {
	files := make([]*app.Item, 0, len(s.fileWithPath))

	for _, f := range s.fileWithPath {
		files = append(files, f)
	}

	return files
}

// RegisterItem adds a file to the stored list of files and
// updates the size of the file to the provided size.
func (s *store) RegisterItem(path string, size int64, modTime time.Time) {
	f := s.fileInfo(path)
	f.Size = size
	f.MTime = modTime
	return
}

func (s *store) RegisterItemTags(path string, tags tag.List) {
}

// RegisterItemHash adds a file to the stored list of files and
// updates the hash of the file to the provided hash.
func (s *store) RegisterItemHash(path, algo, hash string) {
	f := s.fileInfo(path)
	f.Hash[algo] = hash
	return
}

// fileInfo returns the FileInfo object for a file.
// FileInfo objects are cached and only created for new paths
func (s *store) fileInfo(path string) *app.Item {
	f := s.fileWithPath[path]
	if f == nil {
		slash := strings.LastIndex(path, "/")
		name := path[slash+1:]
		// fmt.Printf("trimm 0 .. %v in %v (%v)\n", slash, path, name)
		f = app.NewItem()
		f.Path = path
		f.Name = name
		s.fileWithPath[path] = f
	}
	return f
}
