package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"

	// see https://blog.carlana.net/post/2023/golang-git-hash-how-to/
	// replicated in vendor/ and modified to use time.RFC3339
	"github.com/carlmjohnson/versioninfo"

	"codeberg.org/japh/tix/internal/adapter/fs"
	"codeberg.org/japh/tix/internal/adapter/hash"

	// store "codeberg.org/japh/tix/internal/adapter/store/inmem"
	store "codeberg.org/japh/tix/internal/adapter/store/sqlite"
	"codeberg.org/japh/tix/internal/app"
)

var (
	// we spend most of our time waiting for I/O
	// over 1000 hashers works locally, but causes I/O errors when accessing file over a network
	// anything above 2 simultaneous connections seems to cause problems on a Mac / Samba file system
	maxHashers = 4

	// pick the fastest hash algorithm, assuming their quality is the same
	// Sample speeds:
	//                      | mac / local | mac => samba  | freebsd zfs |
	//      md5:            | 560 MB/s    | --- MB/s      | 375 MB/s    |
	//      sha1:           | 603 MB/s    | 118 MB/s      | 364 MB/s    |
	//      sha256:         | 612 MB/s    | 108 MB/s      | 370 MB/s    | <- preferred
	//      shake256:       | 520 MB/s    | 115 MB/s      | 300 MB/s    |
	hashAlgorithm = "sha256"

	// dbfile is the path to the sqlite database used to store data scraped from the file system.
	//
	// IMPORTANT
	//
	// As of 2023-01-05 I am unaware of any way to detect files with changed extended attributes.
	//
	// i.e.: writing to a file changes it's mtime timestamp, however, xattrs
	// and e.g. hashes of files need to be re-scraped to get updates.
	// This is of course very expensive, so we only do it at obvious, and
	// opportune moments, such as loading the tags of new files and only
	// hashing files with size-conflicts.
	//
	// Updating existing hashes and/or tags needs to be triggered on demand
	//
	dbFile = ".tix.db"

	// | path                            | description                                    |
	// | ------------------------------- | ---------------------------------------------- |
	// | $XDG_CONFIG_HOME/.tix/config.db | central tix configuration                      |
	// | {source_dir}/.tix/              | local index directory for {source_dir}         |
	// | $XDG_CONFIG_HOME/.tix/{id64}    | central index for directory referenced by id64 |
	// | {index_dir}/index.db            | index for directory referenced by id64         |
	// | {index_dir}/{id64}/             | directory for item {id64}'s meta data          |

)

func main() {
	flag.IntVar(&maxHashers, "maxHashers", maxHashers, "the number of goroutines that are allowed to run concurrently")
	flag.IntVar(&maxHashers, "mh", maxHashers, "")
	flag.StringVar(&hashAlgorithm, "algorithm", hashAlgorithm, "the number of goroutines that are allowed to run concurrently")
	flag.StringVar(&hashAlgorithm, "algo", hashAlgorithm, "")

	versioninfo.AddFlag(nil)

	flag.Parse()

	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s <path>\n", os.Args[0])
		fmt.Println()
		fmt.Printf("<path>/%s will be created / updated automatically\n", dbFile)
		os.Exit(1)
	}

	root, err := filepath.Abs(os.Args[1])
	if err != nil {
		panic(fmt.Sprintf("failed to determin absolute path of %q", os.Args[1]))
	}

	hb := hash.New(hashAlgorithm)

	store := store.New(
		store.WithRootDir(root),
		store.WithDBFile(dbFile),
	)
	defer store.Close()

	app := app.New(
		app.WithMaxHashers(maxHashers), // TODO: auto-configure by tracking rate of throughput
		app.WithStore(store),
		app.WithWalker(fs.Walker(store)),
		app.WithHasher(fs.New(hb)),
	)

	app.Walk(root)
}
