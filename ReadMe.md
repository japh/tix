# TIX - An Indexer for Tagged Files

`tix` is a program that creates an index of your files, with special attention
to their _tags_.

## Background

Apple has long provided tags for files in their filesystems, even with colors.
Some typical examples:
  - you can mark a file with a red tag called `important` 
  - or mark your spreadsheet with the tags `todo` and `2023`,
    to remind you that you really need to get your taxes done.

But how do you find those files again? Or edit the tags?
Especially now that you have re-located them to your shiny new NAS?

`tix` aims to help, by building an independent index of your files which you
can then view, search and maintain on any system with a web-browser, not just
MacOS.

This also means that your tags will continue to work even if Apple should stop
supporting file tagging at some time in the future!

## Features

- scan the file metadata in a collection of files, looking for
  - file names & paths
  - file tags (incl. their colors)
  - modification dates

- detect duplicate files (via size and a SHA hash)
  - duplicate files are grouped together so that they can be easily found and
    compared (even though the chance that they will be different is truly
    miniscule, whether you consider two files to be `identical` is _your_
    decision, not mine!) 
  - allow duplicate files to be reduced to a single copy
    (by deleting or hard-linking some or all of the duplicates together)
  - merging all tags from all copies of the file
    - and keeping the order of tags as much as possible

- allow editing of tags
  - replace edited tags on all effected files

- allow normalisation of tags
  - if multiple spellings were used for a single concept, provide a mapping
    from the wrong spelling to the correct spelling

## Motivation

tix attempts to pick up where Apple left off.

### Problems

MacOS X has long provided the ability to tag files, however, over the years,
Apple's support for their tagging system is lacking...

  - editing the Finder's 'tag list' is anything but easy
    - tags are not sorted in the editor
    - tags are not searchable in the editor
  - the MacOS X Finder only knows about tags that were applied to files
    _locally, on that system_
    - tags on remote file systems (e.g. NAS) are displayed but not suggested
    - tags on shared file systems are not sortable / indexed
  - there is no way to clean up tags, e.g.
    - normalise the case of tags, e.g. `todo` => `TODO`
    - update tags on all files at the same time
  - searching for files by tag does not work on remote file systems

### Solutions

Tix tries to address these problems by:

  - extracting file information into an independent index
    - an SQLite database, which is just a file
  - providing a web-UI to access the files
    - images and videos can be viewed directly in any web broswer
    - other file types can be downloaded, or their paths can be copied to allow
      them to be located in the Finder or opened.
    - searching for files
      - by one or more tags (incl. tag exclusion)
      - by filename matching
      - with similar modification dates
    - tags and file names can be manipulated
      - via drag & drop
      - updates apply to all (selected) files instantly
    - changes made in the UI are written back to the file system
      - tags _should_ appear in the Finder
        (once MacOS notices that they have changed)

## Technologies

### Extended Attributes

Apple stores user-defined tags as an extended attribute on each
tagged file, also known as `xattr`.

Apple also indexes file tags in their metadata system, but only for
local file systems.

### plists

To make life interesting, apple stores the actual tag information
in a binary plist format, within the
`com.apple.metadata:_kMDItemUserTags` xattr attribute.

## License

MIT License

## Copyright

2022 - 2024 Stephen Riehm
