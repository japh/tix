#!/bin/sh

#
# Problem:
#
# apple "verifies the contents" of each downloaded file before opening it.
# this can be a real pain, since MacOS also doesn't provide you an easy way of
# accepting the risk and moving on... instead it forces you to either open file
# via the "open" pop-up menu (i.e. not via double click!) or you have to clear
# each file manually in system preferences.
#
# Solution:
# remove the com.apple.quarantine extended attribute from all files :-|
#
# Copyright: Stephen Riehm, 2022
#

system=$(uname -s)
bad_xattr="com.apple.quarantine"

if [[ $system != "Darwin" ]]
then
    echo "$0 only runs on MacOS systems (Darwin, this is ${system})"
    exit 1
fi

find "${0:-.}" -type f -xattrname "${bad_xattr}" -print0 \
     | xargs -0 xattr -d "${bad_xattr}"
