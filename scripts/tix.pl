#!/usr/bin/env perl

#
# Features:
#
#   [x] collect names, sizes and hashes of files
#       [x] only calculate hash for files with the same size
#   [x] persist all information in a database
#   [ ] collect tags & colors
#   [ ] provide the ability to merge tags on files with same size & hash
#   [ ] provide the ability to normalis tags
#       [ ] replace wrong tags with correct tags
#   [ ] allow tags to be given additional info, e.g. verb, name, ...
#       reorder tags: names, verbs, subject/object
#   

#   Index all files, collecting enough information to identify and cleanup
#   duplicate or incorrectly tagged files.
#
#   1. get full list of file names (paths - incl. relative directory)
#   2. get SHA1 hash, size and tags for each path
#      2a. get size of each path
#      2b. get SHA1 hash and tags for each path with a non-unique size
#   3. identify duplicate hashes with identical size
#      3a. show group of duplicate paths with tags
#      3b. prompt for which path(s) to keep (by number)
#      3c. prompt user for tags to keep (merge all, edit)
#      3d. delete all other paths in duplicate group
#
#   Tag Management:
#
#       Automatically merge all tags from all identical files
#
#       Inputs:
#           \w      add to input buffer
#                   highlight/select matching tags
#                   IF no tags match: show short list of potential tags (found from other files)
#           cursor: select next/prev tag
#           delete: remove selected tag(s)
#           return: add selected tag to list
#           escape: cancel input
#
#       Visuals:
#
#           Duplicate Files:
#               1. [ ] path/one/to/first/file
#                          (one) (two) (three)
#               2. [x] path/two/to/second/file
#                          (two) (five)
#               3. [x] path/n/to/nth/file
#                          <no tags>
#
#           Keep file(s): (# or #,#... to keep multiple copies)
#           > 2,3
#
#           Common Tags:
#                   (one) (two) (three) [five]
#
#           1. > f (select five in existing tags)
#           2. > fo (no tags match, show potential matching tags, 1st tag matches)
#              [four] (fourteen) (fourty)
#
#           3. > fourty two (no tags match, space allowed, <return> or ',' to add, <esc> or <C-u> to cancel)
#              [fourty two]
#

#
# Behaviour:
#
#   which files are duplicates?
#   delete selected duplicates (inverse: keep selected duplicates)
#   merge tags from all duplicate copies of a file
#   edit tag list for all copies of a file
#

use strict;
use warnings;

use FindBin qw( $RealBin );
use Digest::SHA;

binmode( STDOUT, ':encoding(UTF-8)' );

# configuration
my @dirs    = @ARGV ? @ARGV : ( '.' );
my $db_file = "$RealBin/.tix.db";

# force output to flush instantly so we can follow progress
our $| = 1;

my $index = tag_index->new(
                          db     => file_index_db->new( db_file => $db_file ),
                          hasher => Digest::SHA->new(),
                          );

$index->load_registered_files();
$index->update_files_info( dirs => \@dirs );
$index->detect_duplicates();
# $index->show_duplicates();

exit;

#----------------------------------------------------------------------
#
# Index package
#

package tag_index;

use strict;
use warnings;

use File::Find;
use Data::Plist;

sub new
    {
    my $class = shift;
    my $param = { @_ };

    my $self = bless {}, $class;
    $self->{db}     = $param->{db};
    $self->{hasher} = $param->{hasher};
    $self->{files}  = {};
    $self->{sizes}  = {};

    return $self;
    }

sub load_registered_files
    {
    my $self = shift;

    foreach my $file ( $self->{db}->registered_files() )
        {
        my $path = $file->{path};
        $self->{files}{$path} = $file;
        }

    printf "loaded %d files from db\n", scalar( keys %{$self->{files}} );

    return;
    }

sub update_files_info
    {
    my $self = shift;

    printf "finding dups in @dirs\n";
    find( { wanted => sub { $self->register_new_file() }, no_chdir => 1 }, @dirs );

    $self->{db}->update_files_in_db( [ values %{$self->{files}} ] );

    return;
    }

sub register_new_file
    {
    my $self = shift;
    my $path = $File::Find::name;

    # skip hidden files and directories completely
    if( $path =~ m{^(.*/)?\.[^/]*$} and not $path eq "." )
        {
        $File::Find::prune = 1;
        return;
        }

    # only interested in files
    if( not -f $path )
        {
        return;
        }

    $path =~ s{^\.\/+}{};

    if( $self->{files}{$path} ) 
        {
        # printf "skipping registered file %s\n", $path;
        return;
        }

    printf "registered %s\n", $path;

    my ( $size, $ctime ) = (stat( _ ))[7,10];
    $self->{files}{$path} = {
                            path  => $path,
                            ctime => $ctime,
                            size  => $size,
                            };
    $self->{sizes}{$size}{$path} = undef;

    return;
    }

sub detect_duplicates
    {
    my $self  = shift;

    my $hasher     = $self->{hasher};
    my $dup_files  = $self->{db}->duplicate_sizes();
    my $update_sth = $self->{db}{st_handle}{update_sha};

    $self->{db}{db_handle}->begin_work();

    my $todo = {
               files => 0,
               size  => 0,
               };
    foreach my $file ( @{$dup_files} )
        {
        next if not -f $file->{path};
        next if $file->{sha1};
        $todo->{files} += 1;
        $todo->{size}  += $file->{size};
        }

    my $start         = time();
    my $pending_count = 0;
    my $hashed        = {
                        files => 0,
                        size  => 0,
                        };
    foreach my $file ( @{$dup_files} )
        {
        my $path = $file->{path};
        if( time() - $start > 10 )
            {
            printf "committing %d files...\n", $pending_count;
            $self->{db}{db_handle}->commit();
            $self->{db}{db_handle}->begin_work();
            $start = time();
            $pending_count = 0;
            }

        if( ! -f $path )
            {
            # printf "no such file %s\n", $path;
            # $todo->{files} -= 1;
            # $todo->{size}  -= $file->{size};
            # $self->{db}->delete_file( $path );
            next;
            }

        if( not $file->{sha} )
            {
            printf "[%s] hashing (%d %s) %s...\n",
                   hash_progress( $hashed, $todo ),
                   human_size( $file->{size} ),
                   $path;
            $hasher->addfile( $path );
            $file->{sha} = $hasher->hexdigest();
            $update_sth->bind_param( ':path', $path );
            $update_sth->bind_param( ':sha', $file->{sha} );
            $update_sth->execute();
            $pending_count++;
            $hashed->{files} += 1;
            $hashed->{size}  += $file->{size};
            }
        else
            {
            # printf "have sha for (%d %s) %s\n", human_size( $file->{size} ), $path;
            }

        my $sha = $file->{sha};
        $self->{sha}{$sha}        //= {};
        $self->{sha}{$sha}{$path}   = undef;
        }

    printf "committing %d files...\n", $pending_count;
    $self->{db}{db_handle}->commit();

    return;
    }

sub duplicate_shas
    {
    my $self = shift;

    my $sha_count = {};
    my @dup_shas;
    foreach my $path ( sort keys %{$self->{files}} )
        {
        my $file = $self->{files}{$path};
        my $sha = $file->{sha};
        next if not $sha;  # file has a unique size
        $sha_count->{$sha} //= 0;
        if( $sha_count->{$sha}++ == 1 )
            {
            push @dup_shas, $sha;
            }
        }

    return @dup_shas;
    }

sub hash_progress
    {
    my $done = shift;
    my $todo = shift;

    return sprintf( "%4d/%4d | %d %s/%d %s | %3d%%", 
                    $done->{files},
                    $todo->{files},
                    human_size( $done->{size} ),
                    human_size( $todo->{size} ),
                    100 * $done->{size} / $todo->{size},
                    );
    }

sub show_duplicates
    {
    my $self = shift;

    my @dup_shas = $self->duplicate_shas();

    foreach my $sha ( @dup_shas )
        {
        printf "---\n";
        my @sha_files = @{$self->{files}}{ keys %{$self->{sha}{$sha}} };

        # TODO: collect all tags related to this sha
        # TODO: print list of tags per file
        # TODO: provide a simple 'merge tags and keep file' option
        # TODO: allow the ability of editing merged tag list

        foreach my $file ( @sha_files )
            {
            printf "%s\n", $file->{path};
            }
        }

    printf "%d duplicates found\n", scalar( @dup_shas );

    return;
    }

sub human_size
    {
    my $size = shift;
    my @scales = qw( b Kb Mb Gb Tb );
    while( $size > 1000 )
        {
        $size /= 1000;
        shift @scales;
        }
    return $size, $scales[0];
    }

#----------------------------------------------------------------------
#
# Wrapper for access to permanent file index storage
# (SQLite DB)
#

package file_index_db;

use strict;
use warnings;

# CPAN modules
use DBI;

sub new
    {
    my $class = shift;
    my $param = { @_ };

    my $self = bless {}, $class;
    $self->{db_file}   = $param->{db_file};
    $self->{db_handle} = undef; # DBI handle
    $self->{st_handle} = undef; # hash of statement handles

    $self->_remove_broken_db();
    $self->_prepare_schema();
    $self->_prepare_statements();

    return $self;
    }

sub DESTROY
    {
    my $self = shift;
    $self->disconnect();
    return;
    }

#----------------------------------------------------------------------
#
# Public API
#

sub connect
    {
    my $self = shift;

    $self->{db_handle} //= DBI->connect(
                                           sprintf( "dbi:SQLite:dbname=%s", $self->{db_file} ),
                                           "",              # user
                                           "",              # password
                                           {                # configuration
                                           RaiseError     => 1,
                                           PrintError     => 1,
                                           sqlite_unicode => 1,
                                           }
                                       );

    return;
    }

sub disconnect
    {
    my $self = shift;
    return $self->{db_handle}->disconnect() if $self->{db_handle};
    }

sub registered_files
    {
    my $self  = shift;

    my $dbh = $self->_dbh();

    my $files = $dbh->selectall_arrayref( q{
                                           select
                                               path,
                                               created,
                                               updated,
                                               size,
                                               sha
                                           from
                                               file_info
                                           },
                                        { Slice => {} },
                                        );

    printf "loaded %d files from db\n", scalar( @{$files} );
    return @{$files};
    }

sub update_files_in_db
    {
    my $self  = shift;
    my $files = shift;

    my $sth = $self->{st_handle}{register_file};

    $self->{db_handle}->begin_work();

    foreach my $file ( @{$files} )
        {
        # printf "updating %s\n", $file->{path};
        $sth->bind_param( ':path',  $file->{path}  );
        $sth->bind_param( ':ctime', $file->{ctime} );   # will be converted to created ISO8601 timestamp
        $sth->bind_param( ':size',  $file->{size}  );
        $sth->execute();
        }

    printf "committing %d files...\n", scalar( @{$files} );
    $self->{db_handle}->commit();
    printf "committed\n";

    return;
    }

sub duplicate_sizes
    {
    my $self = shift;

    # select all files with non-unique sizes
    my $sql = q{
               with
               size_counts as ( select size, count(*) as count from file_info group by size ),
               dup_sizes   as ( select f.path, f.size, f.sha from file_info f join size_counts s on f.size = s.size and s.count > 1 )
               select size, path, sha from dup_sizes order by size, path
               };

    return $self->_dbh()->selectall_arrayref( $sql, { Slice => {} } );
    }

sub files_with
    {
    my $self  = shift;
    my $param = { @_ };

    my $sha = $param->{sha};
    my $sth  = $self->{st_handle}{files_with}
            //= $self->_dbh()->prepare( q{
                                         select
                                             path,
                                             created,
                                             size
                                         from
                                             file_info
                                         where
                                             sha = :sha
                                         order by
                                             path
                                         } );

    $sth->bind_param( ':sha', $sha );
    $sth->execute();

    return $sth->fetchall_arrayref( {} );
    }

#----------------------------------------------------------------------
#
# Internal Methods
#

sub _dbh
    {
    my $self = shift;
    $self->connect() if not defined $self->{db_handle};
    return $self->{db_handle};
    }

sub _prepare_statements
    {
    my $self = shift;

    my $dbh = $self->_dbh();

    $self->{st_handle}{register_file} = $dbh->prepare( q{
                                                        insert or ignore into file_info
                                                               (  path,  created,  size )
                                                        values ( :path, datetime( :ctime, 'unixepoch' ), :size );
                                                        } );

    $self->{st_handle}{update_sha} = $dbh->prepare( q{ update file_info set sha = :sha where path = :path } );

    $self->{st_handle}{remove} = $dbh->prepare( q{ delete from file_info where path = :path } );

    return;
    }

sub _schema_version
    {
    my $self = shift;
    return ( $self->_dbh()->selectrow_array( q{ PRAGMA user_version } ) )[0];
    }

sub _update_schema_version
    {
    my $self = shift;

    my ( $version ) = ( (caller(1))[3] =~ /^file_index_db::schema_(\d+)$/ )
        or die "Don't fiddle with database versions!\n";

    $self->_dbh()->do( qq{ PRAGMA user_version = $version } );

    return $version;
    }

# remove DB file if it exists but has 0 size.
# This can happen if DB creation failed for some reason.
# Problem: SQLite will complain that it can't open the DB if a DB file exists
#          with 0 size.
#          If the DB file doesn't exist, SQLite will create a new one.
# Note:    $dbh->disconnect() MUST be called to ensure that the database is
#          closed properly (see DESTROY())
sub _remove_broken_db
    {
    my $self = shift;

    my $db_file = $self->{db_file};

    unlink $db_file if -f $db_file and not -s _;

    return;
    }

sub _prepare_schema
    {
    my $self = shift;

    my $db_version  = $self->_schema_version();
    my $max_version = 1;
    while( 1 )
        {
        last if not $self->can( sprintf "schema_%d", $max_version + 1 );
        $max_version++;
        }

    my $progress = progress->new( $max_version - $db_version )
        or return;

    while( $db_version < $max_version )
        {
        $db_version++;
        my $updater = sprintf "schema_%d", $db_version;
        $progress->update( sprintf( "updating to schema %d", $db_version ) );
        $self->$updater()
            or $progress->abort( sprintf "Failed to update schema to version %d\n", $db_version );
        }

    $progress->close( sprintf "updated schema to version %d", $db_version );

    return;
    }

#----------------------------------------------------------------------
#
# Schema Definitions
#
# One method per Schema Version
# Each method MUST assume that data has been saved in the previous structure
# and MUST perform all migration actions required to move the data into the new
# schema.
#
sub schema_1
    {
    my $self = shift;

    $self->_dbh()->do( q{
                       create table files (
                           path    text not null,
                           ctime   integer,
                           size    integer,
                           sha1    text,
                           primary key ( path )
                           )
                       } );

    return $self->_update_schema_version();
    }

sub schema_2
    {
    my $self = shift;

    my $dbh = $self->_dbh();

    # new structure, using file_id as primary key instead of path
    $dbh->do( q{
               create table files_new (
                   file_id integer not null,
                   path    text    not null,
                   ctime   integer,
                   size    integer,
                   sha1    text,
                   primary key ( file_id ),
                   unique      ( path    )
                   )
               } );

    # migrate existing data
    $dbh->do( q{
               insert into files_new ( path, ctime, size, sha1 )
               select path, ctime, size, sha1 from files
               } );

    # kill old table
    $dbh->do( q{ drop table files } );

    # rename new table to take the place of the old table
    $dbh->do( q{ alter table files_new rename to files } );

    # done
    return $self->_update_schema_version();
    }

sub schema_3
    {
    my $self = shift;

    my $dbh = $self->_dbh();

    # new structure, using file_id as primary key instead of path
    $dbh->do( q{
               create table files_new (
                   file_id integer  not null,
                   path    text     not null,
                   updated datetime not null default current_timestamp,
                   ctime   integer,
                   size    integer,
                   sha1    text,
                   primary key ( file_id ),
                   unique      ( path    )
                   )
               } );

    # migrate existing data
    $dbh->do( q{
               insert into files_new
                    ( file_id, path, ctime, size, sha1 )
               select file_id, path, ctime, size, sha1 from files
               } );

    # kill old table
    $dbh->do( q{ drop table files } );

    # rename new table to take the place of the old table
    $dbh->do( q{ alter table files_new rename to files } );

    # done
    return $self->_update_schema_version();
    }

sub schema_4
    {
    my $self = shift;

    my $dbh = $self->_dbh();

    # new structure, using file_id as primary key instead of path
    $dbh->do( q{
               create table files_new (
                   file_id integer  not null,
                   path    text     not null,
                   created datetime not null default current_timestamp,
                   updated datetime not null default current_timestamp,
                   size    integer,
                   sha1    text,
                   primary key ( file_id ),
                   unique      ( path    )
                   )
               } );

    # migrate existing data
    $dbh->do( q{
               insert into files_new
                    ( file_id, path, created, updated, size, sha1 )
               select file_id, path, datetime( ctime, 'unixepoch' ), updated, size, sha1 from files
               } );

    # kill old table
    $dbh->do( q{ drop table files } );

    # rename new table to take the place of the old table
    $dbh->do( q{ alter table files_new rename to files } );

    # done
    return $self->_update_schema_version();
    }

sub schema_5
    {
    my $self = shift;

    my $dbh = $self->_dbh();

    # just give the file_info table a better name
    $dbh->do( q{ alter table files rename to file_info } );

    # done
    return $self->_update_schema_version();
    }

sub schema_6
    {
    my $self = shift;

    my $dbh = $self->_dbh();

    # just give the file_info table a better name
    $dbh->do( q{ alter table file_info rename column sha1 to sha } );

    # done
    return $self->_update_schema_version();
    }
 
# sub schema_?
#     {
#     my $self = shift;
# 
#     my $dbh = $self->_dbh();
# 
#     $dbh->do( q{
#                create table tags (
#                     tag_id  integer not null,
#                     tag     text,
#                     color   integer,
#                     primary key ( tag_id )
#                     )
#                } );
# 
#     $dbh->do( q{
#                create table file_tags (
#                     file_id  integer not null,
#                     tag_id   integer not null,
#                     pos      integer,
#                     primary key ( file_id, tag_id )
#                     )
#                } );
# 
#     return $self->_update_schema_version();
#     }

#----------------------------------------------------------------------
#
# very simple progress package for slow updates
#

package progress;

use strict;
use warnings;

sub new
    {
    my $class = shift;
    my $size = shift or return;

    my $self = bless {}, $class;
    $self->{size}  = $size;
    $self->{width} = 60;

    return $self;
    }

sub update
    {
    my $self = shift;
    my $msg  = shift // '';

    $self->{step}++;
    my $done = int( ( $self->{step} * 2 - 1 ) / ( $self->{size} * 2 ) * $self->{width} );

    printf( "\r%-30s [%s%s]",
                $msg,
                '#' x $done,
                ' ' x ( $self->{width} - $done ),
            );

    return;
    }

sub abort
    {
    my $self = shift;
    my $msg  = shift // '';

    die sprintf "\nAborted: %s\n", $msg
    }

sub close
    {
    my $self = shift;
    my $msg  = shift // '';

    printf "\r%-30s [%s]\n", $msg, '#' x $self->{width};

    return;
    }
