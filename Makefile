
APP 		:= tix
TARGETS         := darwin-amd64 freebsd-amd64 linux-amd64 linux-arm-6

.SILENT:

.PHONY: all
all:	build test

#
# Local documentation servers:
#
#   'make start-doc-servers' will start 2 local http servers:
#       - http://localhost:6060     go documentation for this project
#                                   (incl. go's standard packages)
#       - http://localhost:6061     markdown server displaying all files
#                                   in this project
#
#   godoc provides full documentation of all of go's standard packages and the
#   packages within this module.
#       - godoc is a standard component of the go installation.
#
#   gm is a local html server which translates *.md files into nicely
#   formatted html pages.
#       - see https://github.com/kpym/gm
#       - https://github.com/yuin/goldmark is used to parse and format markdown.
#       - unfortunately, gm...:
#           - doesn't let you pick which port to bind to (:8080)
#           - opens the browser whenever you start it (annoying)
#           - is not fully self-contained / offline:
#               - uses livejs directly from https://livejs.com/live.js
#               - uses github's css directly from https://github.com
#       - I'm using a hacked version which I might rollout one day
#       - see also: markdir
.PHONY: start-doc-servers
start-doc-servers:	stop-doc-servers
	# godoc -http :6060 -play -index -notes 'BUG|TODO' &
	godoc -http :6060 -index -notes 'BUG|TODO' &
	gm -s -q --css ~/.config/markdown/index.css --url :6061 &

# kill any running local documentation servers
# Note: preceding '-' ignores errors, i.e.: when the server's aren't running
.PHONY: stop-doc-servers
stop-doc-servers:
	- killall godoc
	- killall gm

# install into your standard $GOBIN or $GOPATH/bin directory
# (typically ~/Go/bin)
.PHONY: install
install: test
	go install -ldflags '$(LDFLAGS)' ./cmd/$(APP)/...

# re-format all .go files
# go vet and golint will complain about typical coding mal-practices
.PHONY: fmt
fmt:
	# better version of go fmt (actually calls go fmt in the background)
	find * -name \*.go -print0 | xargs -0 -n 1 goimports -w
	go vet  ./...
	golint  ./...

# run all unit tests
.PHONY: test
test:
	echo "Unit tests"
	go test ./... # tip: go test -v -run /<pattern>
	# TODO: add schema migration tests, e.g. v1 => v2, with missing files etc

# run all benchmarks
.PHONY: bench
bench:
	echo "Benchmarks"
	go test -bench=. ./...

# build app
.PHONY: build
build:
	echo "Building $(APP)"
	# cross compile:
	# 	GOOS=$(target_os) GOARCH=$(target_arch) GOARM=$(target_arm_version) go build ...
	# 			ARM needs GOARM (e.g., 6 (raspi 3B) or 7)
	# 	see also:
	# 		xgo https://github.com/karalabe/xgo
	# 		go tool dist list
	# 		go env | grep HOST
	go build -o dist/$(APP) ./cmd/$(APP)/...

# WARNING: experimental
.PHONY: dist
dist:	build
	echo "Preparing distribution in dist/"
	cd dist/;                                                         \
	gzip -f $(APP);                                                   \
	gpg --detach-sign --local-user 23A77AC1 --armour --yes $(APP).gz; \
	shasum -a 256 $(APP).gz > $(APP).gz.sha256

# TODO: 2022-01-30 add targets upload to gitea and codeberg

# lightclean: remove cached test results only
.PHONY: lightclean
lightclean:
	echo "Cleaning golang test cache"
	go clean -testcache

# clean: clears the test cache and reports any files which are not part of this
# git repository.
.PHONY: clean
clean: lightclean
	echo "Files not in git:"
	git clean -dxn

# distclean: remove any files which are not part of this git repository
.PHONY: distclean
distclean: lightclean
	echo "Deleting files not in git"
	git clean -dxf
